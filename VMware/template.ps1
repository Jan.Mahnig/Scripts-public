###########################################################################################
# Title:	Get vcenter alerts 
# Filename:	VC_getallerts.ps1     	
# Created by:	Jan Mahnig		
# Date:	1/23/2018				
# Version    1.0  					
###########################################################################################
start-transcript -path c:\scripts\vmware\logs\VC_getallerts.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue


connect-viserver $vcserver  -WarningAction SilentlyContinue
$filelocation = ""


####################
# Define variables #
####################
$filelocation  = "C:\scripts\VMware\Capacity_reports\Capacity_report_"+(get-date -uformat %V)+".html"
$subject = "Template " + (get-date -uformat %V)
$to = ""
$cc = ""

$enablemail = $true
$smtpServer = "" 
$mailfrom = ""
$to_error = ""
$tag_red = "<span style='background-color: red'>"
$tag_yellow = "<span style='background-color: yellow'>"
$style = "<style>BODY{font-family: Arial; font-size: 7pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"



##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To "jan.mahnig@dixonscarphone.com"  -Subject "VM snapshots failed" -SmtpServer $smtpServer -usessl
	stop-transcript
	exit
}


$array = @()
	$object = "" | Select-Object Name,VMState
"test"

##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
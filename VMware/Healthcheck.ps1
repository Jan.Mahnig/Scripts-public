###########################################################################################
# Title:	VMware health check 
# Filename:	Healthcheck.ps1      	
# Created by:	Jan Mahnig		
# Date:	05.06.2018			
# Version: 1.2.0		
# Update: added memory details (ballonedm compressed, shared etc)
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}

if (!(test-path C:\scripts\vmware\healthchecks\)){
	new-item -path C:\scripts\vmware\healthchecks -type Directory
}
start-transcript -path C:\scripts\VMware\logs\healthcheck.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue


connect-viserver $vcserver -WarningAction SilentlyContinue

####################
# Define variables #
####################
$subject = "VMware daily healthcheck " + (get-date -uformat  %d.%m.%Y)
$enablemail = $false
$smtpServer = "" 
$mailfrom = ""
$cc = ""
$to = ""
$to_error = ""
$filelocation = "C:\scripts\vmware\healthchecks\healthcheck" + (get-date -UFormat %d%m) + ".html"
$alert_filelocation = "C:\Scripts\VMware\healthchecks\alarms.csv"
$tag_yellow = "<span style='background-color: yellow'>"
$style = "<style>BODY{font-family: Arial; font-size: 7pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"

##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "VMware health check script failed" -SmtpServer smtp.dixons.co.uk -usessl
	stop-transcript
	exit
}

#############################
# Add Text to the HTML file #
#############################
ConvertTo-Html -title "VMware Health Check " -Head $style -body "<H2>VMware Health script</H2>"  | Out-File $filelocation
ConvertTo-Html -title "VMware Health Check " -body "<H4>Date and time</H4>",(get-date)  | Out-File -Append $filelocation

##################### 
# ESXi hosts status #
#####################
$array = @()
$esxi_hosts = get-vmhost
$stats = "mem.vmmemctl.maximum","mem.shared.maximum","mem.swapused.maximum","mem.compressed.average", "mem.active.average", "mem.consumed.average", "cpu.usage.average"
$esxi_hosts | ForEach-Object {
	$esxi_stats = $_ |get-stat -start ((get-date).adddays(-1))  -stat $stats
	$object = "" | Select-Object Name, Cluster, ConnectionState, OverallStatus, NumCPU,CPUusagePerc,MemActiveGB, MemConsumedGB, MemTotalGB, MemSharedGB, MemCompressedGB, MemBalloonedGB, MemSwappedGB, Version
	$object.Name = $_.name
	$object.Cluster = $_.parent
	$object.ConnectionState = $_.ConnectionState
	$object.OverallStatus = $_.ExtensionData.OverallStatus
	$object.NumCPU = $_.NumCPU
	$object.CPUusagePerc = "{0:N0}" -f ((($esxi_stats |Where-Object-Object {$_.MetricId -eq "cpu.usage.average"}) |Measure-Object -average value).average)
	$object.MemTotalGB = "{0:N0}" -f ($_.MemoryTotalGB)
	$object.MemActiveGB = "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.active.average"}) |Measure-Object -maximum value).maximum /1MB)
	$object.MemConsumedGB = "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.consumed.average"}) |Measure-Object -maximum value).Maximum /1MB)
	$object.MemSharedGB =  "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.shared.maximum"}) |Measure-Object -max value).Maximum /1MB)
	$object.MemCompressedGB = "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.compressed.average"}) |Measure-Object -max value).Maximum /1MB)
	$object.MemBalloonedGB = "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.vmmemctl.maximum"}) |Measure-Object -max value).Maximum /1MB)
	$object.MemSwappedGB = "{0:N0}" -f ((($esxi_stats |Where-Object {$_.MetricId -eq "mem.swapused.maximum"}) |Measure-Object -max value).Maximum /1MB)
	$object.Version = $_.Version
	$array += $object
}
$array| Sort-Object name |ConvertTo-Html -body "<H2>ESXi hosts status</H2>"  | Out-File -Append $filelocation


####################################
#Cluster information and thresholds#
####################################
$vms = get-vm
$array = @()
get-cluster |ForEach-Object {
	$object = "" | Select-Object Name, CPU_Usage, CPU_Overcommitment, Memory_Usage, Memory_Overcommitment, Datastore_Overcommitment
	$object.Name = $_.name
	$object.CPU_Usage = "{0:P2}" -f (($_ |get-vmhost |get-stat -stat cpu.usage.average -start (get-date).adddays(-1)  | Measure-Object -Property value -Average).average / 100 )
	$object.CPU_Overcommitment  = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum numcpu).sum / ($_ |get-vmHost |measure-object -Sum NumCpu).sum) + " : 1"
	$object.Memory_Usage = "{0:P0}" -f (($_ |get-vmhost |get-stat -stat mem.usage.average -start (get-date).adddays(-1)  | Measure-Object -Property value -Average).average / 100)
	$object.Memory_Overcommitment   = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum MemoryGB).sum / ($_ |get-vmHost |measure-object -Sum MemoryTotalGB).sum) + " : 1"
	$datastore = $_ | get-datastore |Where-Object {$_.type -eq "vsan"}
	$datastore_provisioned = [math]::round(($datastore.ExtensionData.Summary.Capacity - $datastore.ExtensionData.Summary.FreeSpace + $datastore.ExtensionData.Summary.Uncommitted)/1GB,2)
	$object.Datastore_Overcommitment = "{0:N2}" -f ($datastore_provisioned / $datastore.capacityGB) + " : 1"
	$array += $object
}
$array | Sort-Object Name | ConvertTo-Html  -body "<H2>Cluster information and thresholds</H2>" | Out-File -Append $filelocation


############# 
# Snapshots # 
#############
$array = @()
$vms |get-snapshot |Where-Object {$_.sizeGB -gt 100 -OR $_.created -lt ((get-date).adddays(-7))} |ForEach-Object {
	$object = "" | Select-Object VM, Name, Created,SizeGB, Creator
	$Snap_event = Get-VIEvent -Entity $_.VM -Types Info -Finish $_.Created -MaxSamples 1 | Where-Object-Object {$_.FullFormattedMessage -imatch 'Task: Create virtual machine snapshot'}
	if ($Snap_event -ne $null) {
		$object.creator = $Snap_event.UserName
	}
	else {
		$object.creator = "N/A"
	}
	$object.VM = $_.VM
	$object.Name = $_.Name
	$object.created = $_.Created
	$object.sizegb = "{0:N2}" -f $_.SizeGB
	$array += $object
}
$array | Sort-Object SizeGB | ConvertTo-Html  -body "<H2>Stale snapshots (>7 days, >100MB)</H2>" | Out-File -Append $filelocation


#########################
# Datastore information #
#########################
#############
#vSAN active#
#############
if ((Get-Datastore |Where-Object {$_.type -eq "vsan"}).count -ne 0){
	$vSAN_active = $true
}

#################
# vSAN size info#
#################
$datastores = Get-Datastore
$array = @()
$datastores | ForEach-Object {
	$object = "" | Select-Object Datastore, FreeTB, TotalTB, PercFree
	$object.Datastore = $_.Name
	$object.TotalTB = "{0:N2}" -f ($_.CapacityGB / 1024)
	$object.FreeTB = "{0:N2}" -f ($_.FreeSpaceGB / 1024)
	$object.PercFree = "{0:P0}" -f ($object.FreeTB/$object.TotalTB)
	$array += $object
}
$array | Sort-Object Datastore | ConvertTo-Html  -body "<H2>Datastore space available</H2>"  | Out-File -Append $filelocation

##################################
# vSAN info with one host failure#
##################################
if ($vSAN_active){
	$datastores = Get-Datastore |Where-Object {$_.type -eq "vsan"}
	$array = @()
	$datastores | ForEach-Object {
		$cluster = $_ |get-vmhost |get-cluster
		$vsan_view = Get-VSANView -Id "VsanVcClusterHealthSystem-vsan-cluster-health-system" |Select-Object -first 1
		$cluster_view = $cluster.ExtensionData.MoRef
		$result = $vsan_view.VsanQueryVcClusterHealthSummary($cluster_view,5,$null,$false,$null,$true) 

		$object = "" | Select-Object Datastore, FreeTB, TotalTB, PercFree
		$object.Datastore = $_.name
		$object.FreeTB = "{0:N2}" -f ((($result.LimitHealth.WhatifHostFailures |Where-Object {$_.numfailures -eq 1}).TotalCapacityB - (($result.LimitHealth.WhatifHostFailures |Where-Object {$_.numfailures -eq 1}).TotalUsedCapacityB))/1TB)
		$object.TotalTB = "{0:N2}" -f ((($result.LimitHealth.WhatifHostFailures |Where-Object {$_.numfailures -eq 1}).TotalCapacityB)/1TB)
		$object.PercFree = "{0:P0}" -f ($object.FreeTB / $object.TotalTB)
		$array += $object
	}
	$array | Sort-Object Datastore | ConvertTo-Html  -body "<H2>vSAN datastore space available with one host failures</H2>" | Out-File -Append $filelocation
}

################### 
# vSAN information#
###################
if ($vSAN_active){
	"<H2>vSAN cluster health report</H2>"  |out-file -Append $filelocation
	$datastores |ForEach-Object {
		"<H2>"+ $_.name + "</H2>" |out-file -Append $filelocation
		$cluster = $_ |get-vmhost |get-cluster
		$vsan_view = Get-VSANView -Id "VsanVcClusterHealthSystem-vsan-cluster-health-system" |Select-Object -first 1
		$cluster_view = $cluster.ExtensionData.MoRef
		$result = $vsan_view.VsanQueryVcClusterHealthSummary($cluster_view,5,$null,$false,$null,$true) 
		$array = @()
		$result.groups |ForEach-Object {
			$object = "" |select-object GroupName, GroupHealth
			switch ($_.grouphealth){
				"green" {$object.GroupHealth = "Pass"}
				"yellow" {$object.GroupHealth = "Warning"}
				"red" {$object.GroupHealth = "Error"}
			}
			$object.GroupName = $_.GroupName
			$array += $object
		}
		$array  | ConvertTo-Html -body "<H4>vSAN  Health Status</H4>" | Out-File -Append $filelocation
		$result.Groups.GroupTests |Where-Object {$_.testhealth -ne "green"} |Select-Object TestName, TestDescription, TestHealth | ConvertTo-Html -body "<H4>vSAN Overall Health Issues</H4>"  | Out-File -Append $filelocation
		$array = @()
		$result.ObjectHealth.ObjectHealthDetail |Where-Object {$_.NumObjects -ne 0 -AND $_.Health -ne "healthy"} | ForEach-Object {
			$object ="" |select-object Health,NumObjects
			switch($_.health) {
				"reducedavailabilitywithnorebuild" {$object.Health = "Reduced availability with no rebuild:"}
				"reducedavailabilitywithnorebuilddelaytimer" {$object.Health = "Reduced availability with no rebuild - delay timer"}
				"datamove" {$object.Health = "Data move"}
				"healthy" {$object.Health = "Healthy"}
				"inaccessible" {$object.Health = "Inaccessible"}
				"reducedavailabilitywithactiverebuild" {$object.Health = "Reduced availability"}
				"nonavailabilityrelatedreconfig" {$object.Health = "Non-availability related reconfig"}
				"nonavailabilityrelatedincompliance" {$object.Health = "Non-availability related incompliance"}
			}
			$object.NumObjects = $_.NumObjects
			$array += $object
		}
		if ($array) {
			$array |select-object Health, NumObjects| ConvertTo-Html -body "<H4>vSAN object health status</H4>"  | Out-File -Append $filelocation
		}
	}

##############
#Failed disks#
##############
	if (($result.PhysicalDisksHealth |Where-Object {$_.disks.summaryhealth -ne "green"}).count -ne 0){
		$array = @()
		$result.PhysicalDisksHealth |ForEach-Object {
		#for cycle for hosts
		$object = "" |select-object Hostname,Name, SummaryHealth, OperationalHealthDescription
		$object.hostname = $_.hostname
		$_.disks | ForEach-Object {
			#for cycle for disks
			if ($_.summaryhealth -ne "green") {
				$object.name = $_.name
				$object.SummaryHealth = $_.SummaryHealth
				$object.OperationalHealthDescription = $_.OperationalHealthDescription
				$array += $object
			}			
		}
	}
	$array | ConvertTo-Html -body "<H4>vSAN capacity tier disks with issue</H4>"  | Out-File -Append $filelocation
	}
#################
#vSAN congestion#
#################
} #finished vsan part

##################
#Triggered alarms#
##################
$csv = import-csv $alert_filelocation  
$csv |Sort-Object name, alarm, status -Unique |Sort-Object name | ConvertTo-Html -body "<H2>Triggered vCenter alarms</H2>"  | Out-File -Append $filelocation
Remove-Item $alert_filelocation -force

##################
# VM information #
##################
$array = @()
 get-folder -type VM |Where-Object {$_.name -notlike "*nsx*" -AND $_.name -notlike "*templates*" -AND $_.name -notlike "project.use" -AND $_.name -notlike "*vm*" } |get-vm |Where-Object {$_.powerstate -eq "PoweredOn"} |ForEach-Object {
	$vm_view = $_ |get-view
	$VMdisk = $_ |get-vmguest |Select-Object -ExpandProperty disks
	if (!($VMdisk)){
		$VMdisk = $null
	}
	$Freespace = ((1 - ($VMdisk | Measure-Object -Sum freespacegb -erroraction silentlycontinue).sum / ($VMdisk |Measure-Object -Sum CapacityGB -erroraction silentlycontinue).sum) * 100)
	$CPU_usage = $vm_view.summary.quickstats.OverallCpuUsage / $vm_view.summary.runtime.maxcpuusage * 100
	$Memory_usage = $vm_view.summary.quickstats.GuestMemoryUsage / $vm_view.summary.runtime.MaxMemoryUsage * 100
	if ($Freespace -gt 80 -OR $Memory_usage -gt 80 -OR $CPU_usage -gt 80 -OR $vm_view.OverallStatus -ne "green" -OR $vm_view.guest.toolsstatus -eq "toolsNotInstalled" -OR $vm_view.guest.toolsstatus -eq "toolsNotRunning") {
		$object = "" | Select-Object Name,VMState, OS, IPAddress, TotalCPU, TotalMemory, TotalDisk, CPU_usage, Memory_usage, Freespace, ToolsStatus, ToolsVersion, OverallStatus
		$object.name = $_.name
		$object.IPAddress = $vm_view.guest.ipAddress |Select-Object -first 1
		$object.os = $vm_view.guest.guestfullname
		$object.VMState = $vm_view.summary.runtime.powerState
		$object.TotalDisk = "{0:N2}" -f ((Get-HardDisk -VM $_ | Measure-Object -Sum CapacityGB).Sum)
		$object.TotalCPU = $vm_view.summary.config.numcpu
		$object.TotalMemory = $vm_view.summary.config.memorysizemb	
		
		if ($Memory_usage -ge '80'){
			$object.Memory_usage = $tag_yellow + ("{0:N2}" -f $Memory_usage)
		}
		else {
			$object.Memory_usage = "{0:N2}" -f $Memory_usage
		}
		
		if ($CPU_usage -ge '80'){
			$object.CPU_usage = $tag_yellow + ("{0:N2}" -f $CPU_usage)
		}
		else {
			$object.CPU_usage = "{0:N2}" -f $CPU_usage
		}
		
		if ($Freespace -ge '80'){
			$object.Freespace = $tag_yellow + ("{0:N2}" -f $Freespace)
		}
		else {
			$object.Freespace = "{0:N2}" -f $Freespace
		}
		
		if ($vm_view.OverallStatus -ne "green"){
			$object.overallstatus = $tag_yellow + ($vm_view.OverallStatus)
		}
		else {
			$object.overallstatus = $vm_view.OverallStatus
		}
		
		if ($vm_view.guest.toolsstatus -eq "toolsNotInstalled" -OR $vm_view.guest.toolsstatus -eq "toolsNotRunning" -OR $vm_view.guest.toolsstatus -eq "toolsOld"){
			$object.ToolsStatus = $tag_yellow + ($vm_view.guest.toolsstatus)
		}
		else {
			$object.ToolsStatus = $vm_view.guest.toolsstatus
		}
		$array += $object
	}
}
$report = $array | ConvertTo-Html -body "<H2>Virtual Machines with issue or alert (> 80% CPU, Memory, HDD, alert, VMtools not running)</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation

################# 
#Connected CDROM# 
#################
$vms | Where-Object { $_ | get-cddrive | Where-Object { $_.ConnectionState.Connected -eq "true" } } | Select-Object Name | ConvertTo-Html -body "<H2>CDROMs connected</H2>" |Out-File -Append $filelocation

########################## 
# Connected floppy drives#
##########################
$vms | Where-Object { $_ | get-floppydrive | Where-Object { $_.ConnectionState.Connected -eq "true" } } | Select-Object Name |ConvertTo-Html -body "<H2>Floppy drives connected</H2>"  |Out-File -Append $filelocation

######################
# E-mail HTML output #
######################
if ($enablemail) { 
	if (test-path $filelocation) {
		#odeslani reportu
		Send-MailMessage -From $mailfrom  -To $to -cc $cc -Subject $subject -SmtpServer $smtpServer -Attachments $filelocation -usessl -body (get-content $filelocation |out-string) -BodyAsHtml
	}
	else
	{
		Send-MailMessage -From $mailfrom -To $to_error  -Subject "VMware health check script failed" -SmtpServer $smtpServer -usessl
	}
}

##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
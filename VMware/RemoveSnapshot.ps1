###########################################################################################
# Title:	Remove snapshots
# Filename:	RemoveSnapshots.ps1     	
# Created by:	Jan Mahnig		
# Data   11.06.2018
# Version 1.0				
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}
start-transcript -path C:\scripts\VMware\logs\RemoveSnapshots.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]


##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue


connect-viserver $vcserver -WarningAction SilentlyContinue


####################
# Define variables #
####################

$smtpServer = "" 
$mailfrom = ""
$to_error = ""

##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "RemoveSnapshots.ps1" -SmtpServer $smtpserver -usessl
	stop-transcript
	exit
}

####################################
#Remove snapshots older -gt 14 days#
####################################
get-vm |get-snapshot |where {$_.created -lt ((get-date).adddays(-14))} |remove-snapshot -confirm:$false -Verbose


##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
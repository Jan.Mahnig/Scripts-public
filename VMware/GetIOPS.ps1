$array = @()
get-cluster |get-vm |ForEach-Object {
$object = "" | Select-Object Name, Min, Max, Average

$stats = Get-VsanStat -Entity $_ -Name Performance.ReadIops -StartTime ((get-date).Addminutes(-15)) |Measure-Object -Average -Maximum -Minimum value
$object.name = $_.name
$object.min = $stats.minimum
$object.max = $stats.maximum
$object.average = $stats.average
$array += $object

} 

$array 
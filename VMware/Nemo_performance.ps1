$array = @()
get-vm *test04.sl* |where {$_.PowerState -eq "poweredon"} |ForEach-Object {
    $object = "" | Select-Object Name, Mem_active_current,Mem_active_max_MB, Mem_consumed_current, Mem_prov, CPU_usage_average, CPU_usage_max, CPU_alloc,CPU_alloc_MHz, Space_free, Space_provisioned

    #$stats = Get-stats -Entity $_ -Name Performance.ReadIops -StartTime ((get-date).Addminutes(-15)) |Measure-Object -Average -Maximum -Minimum value
    $object.name = $_.name
    $object.Mem_active_max_MB = "{0:N0}" -f (($_  |get-stat -Stat mem.active.average -Finish (get-date).addhours(-8) |Measure-Object -Maximum value).maximum /1024)
    $object.Mem_active_current = ($_ |get-view).summary.QuickStats.GuestMemoryUsage 
    $object.Mem_consumed_current = ($_ |get-view).summary.QuickStats.HostMemoryUsage
    $object.Mem_prov = $_.MemoryGB
    $object.CPU_usage_average = "{0:N0}" -f (($_  |get-stat -Stat cpu.usage.average -Finish (get-date).addhours(-8) |Measure-Object -Average value).average)
    $object.CPU_usage_max = "{0:N0}" -f (($_  |get-stat -Stat cpu.usage.average -Finish (get-date).addhours(-8) |Measure-Object -Maximum value).maximum)
    $object.CPU_alloc = $_.numcpu
    $object.CPU_alloc_MHz = ($_ |get-view ).runtime.maxcpuusage
    $object.Space_free = "{0:P0}" -f ((($_ |get-view).guest.disk |measure-object -Sum freespace).sum / (($_ |get-view).guest.disk |measure-object -Sum capacity).sum)
    $object.Space_provisioned = "{0:N0}" -f ((($_ |get-view).guest.disk |measure-object -Sum capacity).sum /1GB) 
    $array += $object
} 

$array |Format-Table
$array = @()
get-vm |get-snapshot |where {$_.sizeGB -gt 100 -OR $_.created -lt ((get-date).adddays(-7))} |foreach {
	$object = "" | Select-Object VM, Name, Created,SizeGB, Creator
	$Snap_event = Get-VIEvent -Entity $_.VM -Types Info -Finish $_.Created -MaxSamples 1 | Where-Object {$_.FullFormattedMessage -imatch 'Task: Create virtual machine snapshot'}
	if ($Snap_event -ne $null) {
		$object.creator = $Snap_event.UserName
	}
	else {
		$object.creator = "N/A"
	}
	$object.VM = $_.VM
	$object.Name = $_.Name
	$object.created = $_.Created
	$object.sizegb = "{0:N2}" -f $_.SizeGB
	$array += $object
}
$array | Sort-Object SizeGB 
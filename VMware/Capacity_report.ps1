###########################################################################################
# Title:	VMware Capacity report
# Filename:	Capacity_report.ps1     	
# Created by:	Jan Mahnig		
# Data   05.06.2018
# Version 2.0				
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}

if (!(test-path C:\scripts\vmware\Capacity_reports\)){
	new-item -path C:\scripts\vmware\Capacity_reports -type Directory
}
start-transcript -path C:\scripts\VMware\logs\Capacity_report.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]


##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue

connect-viserver $vcserver  -WarningAction SilentlyContinue


####################
# Define variables #
####################
$filelocation  = "C:\scripts\VMware\Capacity_reports\Capacity_report_"+(get-date -uformat %V)+".html"
$subject = "Capacity report week " + (get-date -uformat %V)
$to = ""
$cc = ""

$enablemail = $false
$smtpServer = "" 
$mailfrom = ""
$to_error = ""
$tag_red = "<span style='background-color: red'>"
$tag_yellow = "<span style='background-color: yellow'>"
$style = "<style>BODY{font-family: Arial; font-size: 7pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"




##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "Capacity report failed" -SmtpServer $smtpserver -usessl
	stop-transcript
	exit
}

################
#Basic overview#
################
$array_ov = @()
get-cluster |ForEach-Object {
	$object = "" |Select-Object Name, usageCPU, usageMemory, usagevSAN
	$object.Name = $_.Name
	$object.usageCPU = ($_ |get-vmhost |get-stat -stat cpu.usage.average -start (get-date).adddays(-7)  | Measure-Object -Property value -Average).average / 100 
	$object.usageMemory = ((get-stat -Entity $_ -stat mem.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average).Average / 100)
	
	if (($_ |get-vmhost |Where-Object {$_.powerstate -ne "PoweredOn"}).count -ne 0) {
		$vsan_reserve="0"
	}
	else {
		$vsan_reserve = ($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.Capacity / $_.ExtensionData.Host.Count
	}
	$free = (($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.FreeSpace) - $vsan_reserve
	$capacity = (($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.Capacity)  - $vsan_reserve
	$object.usagevSAN = (($capacity - $free)/$capacity)
	
	if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

		if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

	if ($object.usageMemory -ge '0.7' -AND $object.usageMemory -lt '0.9'){
		$object.usageMemory = $tag_yellow + $object.usageMemory.toString("P")
	}
	elseif ($object.usageMemory -ge '0.9') {
		$object.usageMemory = $tag_red + $object.usageMemory.toString("P")
	}
	else{
		$object.usageMemory = $object.usageMemory.toString("P")
	}
	if ($object.usagevSAN -ge '0.65' -AND $object.usagevSAN -lt '0.85'){
		$object.usagevSAN = $tag_yellow + $object.Usage.toString("P")
	}
	elseif ($object.usagevSAN -ge '0.85') {
		$object.usagevSAN = $tag_red + $object.usagevSAN.toString("P")
	}
	else {
		$object.usagevSAN = $object.usagevSAN.toString("P")
	}
	$array_ov += $object
}



#################################
#Gathering cluster and host data#
#################################
$array = @()
(get-vmhost) + (get-cluster) | ForEach-Object {
	$object = "" |Select-Object Name, Type, usageCPU, MaxUsageCPU, CPU_Overcommitment, countCPU, usageMemory, MaxUsageMemory, Memory_Overcommitment, MemorySizeGB, totalVM, totalVMRunning, totalVMoff
	$object.Name = $_.Name
	if ($_.ExtensionData.Gettype().name -eq "ClusterComputeResource"){
		$object.type = "Cluster"
	}
	elseif ($_.ExtensionData.Gettype().name -eq "HostSystem") {
		$object.type = "Host"
	}
	$memory = (get-stat -Entity $_ -stat mem.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average -Maximum)
	$object.usageMemory = ($memory.Average / 100)
	$object.MaxUsageMemory = ($memory.Maximum / 100).toString("P")
	if ($object.type -eq "Host" ){
		#ESXI
		$CPU =  (get-stat -Entity $_ -stat cpu.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average -Maximum)
		$object.MaxUsageCPU = ($CPU.Maximum / 100).toString("P")
		$object.countCPU = $_.numCPU
		$object.MemorySizeGB = [int]$_.MemoryTotalGB
		$object.usageCPU =($CPU.Average / 100)
	}
	elseif ($object.type -eq "Cluster") {
		#cluster
		$object.countCPU = $_.ExtensionData.Summary.NumCpuCores
		$object.MemorySizeGB = [int]($_.ExtensionData.Summary.TotalMemory /1GB)
		$object.usageCPU = ($_ |get-vmhost |get-stat -stat cpu.usage.average -start (get-date).adddays(-7)  | Measure-Object -Property value -Average).average / 100 
		#$object.usageCPU = "N/A"
		$object.MaxUsageCPU	= "N/A"
	}
	$object.Memory_Overcommitment   = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum MemoryGB).sum / $object.MemorySizeGB) + " : 1"
	$object.CPU_Overcommitment  = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum numcpu).sum / $object.countCPU ) + " : 1"
	$object.totalVM = ($_ |get-vm).count
	$object.totalVMoff = ($_ |get-vm |Where-Object {$_.powerstate -eq "poweredoff"}).count
	$object.totalVMRunning = ($_ |get-vm |Where-Object {$_.powerstate -eq "poweredon"}).count

	if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

	if ($object.MaxUsageCPU -ge '0.70' -AND $object.MaxUsageCPU -lt '0.85'){
		$object.MaxUsageCPU = $tag_yellow + $object.MaxUsageCPU.toString("P")
	}
	elseif ($object.MaxUsageCPU -ge '0.85') {
		$object.MaxUsageCPU = $tag_red + $object.MaxUsageCPU.toString("P")
	}
	else {
		$object.MaxUsageCPU = $object.MaxUsageCPU.toString("P")
	}

	if ($object.usageMemory -ge '0.7' -AND $object.usageMemory -lt '0.9'){
		$object.usageMemory = $tag_yellow + $object.usageMemory.toString("P")
	}
	elseif ($object.usageMemory -ge '0.9') {
		$object.usageMemory = $tag_red + $object.usageMemory.toString("P")
	}
	else{
		$object.usageMemory = $object.usageMemory.toString("P")
	}

	if ($object.MaxUsageMemory -ge '0.7' -AND $object.MaxUsageMemory -lt '0.9'){
		$object.MaxUsageMemory = $tag_yellow + $object.MaxUsageMemory.toString("P")
	}
	elseif ($object.MaxUsageMemory -ge '0.9') {
		$object.MaxUsageMemory = $tag_red + $object.MaxUsageMemory.toString("P")
	}
	else{
		$object.MaxUsageMemory = $object.MaxUsageMemory.toString("P")
	}

	$array += $object
} 

##########################
#Gathering datastore data#
##########################
$array_ds = @()
get-datastore  |Where-Object {$_.name -notlike "*local*"} |ForEach-Object {
	$object = "" |Select-Object Name, Type, FreeSpaceGB,FreeSpaceRaw, Usage, ProvisionedGB,ProvisionedRaw, CapacityGB,CapacityRaw, Overcommitment
	$object.name = $_.Name
	$object.type = $_.Type
	$object.FreeSpaceGB =  [int]($_.ExtensionData.Summary.FreeSpace /1GB)
	$object.CapacityGB =  [int]($_.ExtensionData.Summary.Capacity /1GB)
	$object.FreeSpaceRaw = "N/A"
	$object.ProvisionedRaw = "N/A"
	$object.CapacityRaw = "N/A"
	$object.ProvisionedGB = [int](($_.ExtensionData.Summary.Capacity  - $_.ExtensionData.Summary.FreeSpace + $_.ExtensionData.Summary.Uncommitted)/1GB)
	
	if ($object.type -eq "vsan"){
		$object.FreeSpaceRaw = $object.FreeSpaceGB
		$object.ProvisionedRaw = $object.ProvisionedGB
		$object.CapacityRaw = $object.CapacityGB
		if (($_ |get-vmhost |Where-Object {$_.powerstate -ne "PoweredOn"}).count -ne 0) {
			$vsan_reserve="0"
		}
		else {
			$vsan_reserve = $object.CapacityGB / ($_.ExtensionData.Host.Count)
		}

		$object.FreeSpaceGB =  [int]($object.FreeSpaceGB - $vsan_reserve) / 2
		$object.CapacityGB =  [int]($object.CapacityGB - $vsan_reserve) / 2
		$object.ProvisionedGB = $object.ProvisionedGB / 2
	}
	$object.Overcommitment =  ($object.ProvisionedGB / $object.CapacityGB)
	$object.Usage = (($object.CapacityGB - $object.FreeSpaceGB) / $object.CapacityGB)
	#barvicky
	if ($object.Usage -ge '0.65' -AND $object.Usage -lt '0.85'){
		$object.Usage = $tag_yellow + $object.Usage.toString("P")
	}
	elseif ($object.Usage -ge '0.85') {
		$object.Usage = $tag_red + $object.Usage.toString("P")
	}
	else {
		$object.Usage = $object.Usage.toString("P")
	}

	if ($object.Overcommitment -ge '1' -AND $object.Overcommitment -lt '1.50'){
		$object.Overcommitment = $tag_yellow + $object.Overcommitment.toString("P")
	}
	elseif ($object.Overcommitment -ge '1.50') {
		$object.Overcommitment = $tag_red + $object.Overcommitment.toString("P")
	}
	else{
		$object.Overcommitment = $object.Overcommitment.toString("P")
	}
	$array_ds += $object
}


#############
#Create HTML#
#############
"Dear All,<br><br>This is automatically generated CapacityGB report of Softlayer clusters and hosts on weekly bases. <br><br>"  |out-file $filelocation

$report = $null
$report = $array_ov |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Basic overview</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation

$report = $null
$report = $array |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Cluster and host data</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")

$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation
$report = $null
$report = $array_ds |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Datastore data</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation
"<H3>Legenda:</H3>
CPU_Overcommitment - Ratio between physical CPU and assigned virtual CPU<br>
countCPU - Physical cores without HyperThreading<br>
Memory_Overcommitment - Ratio between allocated/provisioned memory and host memory<br>
FreeSpaceGB - Data usable for virtual machines and data. For vSAN divided by 2 and minus one host reserve<br>
FreeSpaceRaw - Raw capacity visible from vcenter and by hosts. Only for vSAN datastores <br>
Datastore overcommitment - Ratio between provisioned storage to virtual machines and datastore capacity. ProvisionedGB/CapacityGB. <br>
<H3>Thresholds: </H3>
CPU usage: Warning threshold 70%. Error threshold 85% <br>
Memory usage: Warning threshold 70%. Error threshold 90% <br>
Datastore usage:  Warning threshold 65%. Error threshold 85% <br>
Datastore overcommitment: Warning threshold 100%. Error threshold 150% <br>"|out-file -FilePath $filelocation -Append
"<br>Best Regards<br><br> " |out-file -FilePath $filelocation -Append

#################
#Convert to HTML#
#################
$body = get-content $filelocation  |out-string



######################
# E-mail HTML output #
######################
if ($enablemail){
	if ($array) {
		Send-MailMessage -From $mailfrom -To $to -CC $cc -Subject $subject -SmtpServer $smtpServer -BodyAsHtml -Body $body -usessl
	}
	else
	{
		Send-MailMessage -From $mailfrom -To $to_error  -Subject "Capacity report failed" -SmtpServer $smtpServer -usessl
	}
}



##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
############################################################################################################
# Title:	VMware Capacity report
# Filename:	Capacity_report.ps1     	
# Created by:	Jan Mahnig		
# Data   05.06.2018
# Version 2.0				
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}

if (!(test-path C:\scripts\vmware\Capacity_reports\)){
	new-item -path C:\scripts\vmware\Capacity_reports -type Directory
}
start-transcript -path C:\scripts\VMware\logs\Capacity_report.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]


##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue

connect-viserver $vcserver  -WarningAction SilentlyContinue


####################
# Define variables #
####################
$filelocation  = "C:\scripts\VMware\Capacity_reports\Capacity_report_"+(get-date -uformat %V)+".html"
$subject = "Capacity report week " + (get-date -uformat %V)
$to = ""
$cc = ""

$enablemail = $false
$smtpServer = "" 
$mailfrom = ""
$to_error = ""
$tag_red = "<span style='background-color: red'>"
$tag_yellow = "<span style='background-color: yellow'>"
$style = "<style>BODY{font-family: Arial; font-size: 7pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"




##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "Capacity report failed" -SmtpServer $smtpserver -usessl
	stop-transcript
	exit
}

################
#Basic overview#
################
$array_ov = @()
get-cluster |ForEach-Object {
	$object = "" |Select-Object Name, usageCPU, usageMemory, usagevSAN
	$object.Name = $_.Name
	$object.usageCPU = ($_ |get-vmhost |get-stat -stat cpu.usage.average -start (get-date).adddays(-7)  | Measure-Object -Property value -Average).average / 100 
	$object.usageMemory = ((get-stat -Entity $_ -stat mem.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average).Average / 100)
	
	if (($_ |get-vmhost |Where-Object {$_.powerstate -ne "PoweredOn"}).count -ne 0) {
		$vsan_reserve="0"
	}
	else {
		$vsan_reserve = ($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.Capacity / $_.ExtensionData.Host.Count
	}
	$free = (($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.FreeSpace) - $vsan_reserve
	$capacity = (($_ |get-datastore |Where-Object {$_.type -eq "vsan"}).ExtensionData.Summary.Capacity)  - $vsan_reserve
	$object.usagevSAN = (($capacity - $free)/$capacity)
	
	if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

		if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

	if ($object.usageMemory -ge '0.7' -AND $object.usageMemory -lt '0.9'){
		$object.usageMemory = $tag_yellow + $object.usageMemory.toString("P")
	}
	elseif ($object.usageMemory -ge '0.9') {
		$object.usageMemory = $tag_red + $object.usageMemory.toString("P")
	}
	else{
		$object.usageMemory = $object.usageMemory.toString("P")
	}
	if ($object.usagevSAN -ge '0.65' -AND $object.usagevSAN -lt '0.85'){
		$object.usagevSAN = $tag_yellow + $object.Usage.toString("P")
	}
	elseif ($object.usagevSAN -ge '0.85') {
		$object.usagevSAN = $tag_red + $object.usagevSAN.toString("P")
	}
	else {
		$object.usagevSAN = $object.usagevSAN.toString("P")
	}
	$array_ov += $object
}



#################################
#Gathering cluster and host data#
#################################
$array = @()
(get-vmhost) + (get-cluster) | ForEach-Object {
	$object = "" |Select-Object Name, Type, usageCPU, MaxUsageCPU, CPU_Overcommitment, countCPU, usageMemory, MaxUsageMemory, Memory_Overcommitment, MemorySizeGB, totalVM, totalVMRunning, totalVMoff
	$object.Name = $_.Name
	if ($_.ExtensionData.Gettype().name -eq "ClusterComputeResource"){
		$object.type = "Cluster"
	}
	elseif ($_.ExtensionData.Gettype().name -eq "HostSystem") {
		$object.type = "Host"
	}
	$memory = (get-stat -Entity $_ -stat mem.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average -Maximum)
	$object.usageMemory = ($memory.Average / 100)
	$object.MaxUsageMemory = ($memory.Maximum / 100).toString("P")
	if ($object.type -eq "Host" ){
		#ESXI
		$CPU =  (get-stat -Entity $_ -stat cpu.usage.average -start (get-date).adddays(-7) -MaxSamples 100| Measure-Object -Property value -Average -Maximum)
		$object.MaxUsageCPU = ($CPU.Maximum / 100).toString("P")
		$object.countCPU = $_.numCPU
		$object.MemorySizeGB = [int]$_.MemoryTotalGB
		$object.usageCPU =($CPU.Average / 100)
	}
	elseif ($object.type -eq "Cluster") {
		#cluster
		$object.countCPU = $_.ExtensionData.Summary.NumCpuCores
		$object.MemorySizeGB = [int]($_.ExtensionData.Summary.TotalMemory /1GB)
		$object.usageCPU = ($_ |get-vmhost |get-stat -stat cpu.usage.average -start (get-date).adddays(-7)  | Measure-Object -Property value -Average).average / 100 
		#$object.usageCPU = "N/A"
		$object.MaxUsageCPU	= "N/A"
	}
	$object.Memory_Overcommitment   = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum MemoryGB).sum / $object.MemorySizeGB) + " : 1"
	$object.CPU_Overcommitment  = "{0:N2}" -f (($_ | get-vm |Measure-Object -Sum numcpu).sum / $object.countCPU ) + " : 1"
	$object.totalVM = ($_ |get-vm).count
	$object.totalVMoff = ($_ |get-vm |Where-Object {$_.powerstate -eq "poweredoff"}).count
	$object.totalVMRunning = ($_ |get-vm |Where-Object {$_.powerstate -eq "poweredon"}).count

	if ($object.usageCPU -ge '0.70' -AND $object.usageCPU -lt '0.85'){
		$object.usageCPU = $tag_yellow + $object.usageCPU.toString("P")
	}
	elseif ($object.usageCPU -ge '0.85') {
		$object.usageCPU = $tag_red + $object.usageCPU.toString("P")
	}
	else {
		$object.usageCPU = $object.usageCPU.toString("P")
	}

	if ($object.MaxUsageCPU -ge '0.70' -AND $object.MaxUsageCPU -lt '0.85'){
		$object.MaxUsageCPU = $tag_yellow + $object.MaxUsageCPU.toString("P")
	}
	elseif ($object.MaxUsageCPU -ge '0.85') {
		$object.MaxUsageCPU = $tag_red + $object.MaxUsageCPU.toString("P")
	}
	else {
		$object.MaxUsageCPU = $object.MaxUsageCPU.toString("P")
	}

	if ($object.usageMemory -ge '0.7' -AND $object.usageMemory -lt '0.9'){
		$object.usageMemory = $tag_yellow + $object.usageMemory.toString("P")
	}
	elseif ($object.usageMemory -ge '0.9') {
		$object.usageMemory = $tag_red + $object.usageMemory.toString("P")
	}
	else{
		$object.usageMemory = $object.usageMemory.toString("P")
	}

	if ($object.MaxUsageMemory -ge '0.7' -AND $object.MaxUsageMemory -lt '0.9'){
		$object.MaxUsageMemory = $tag_yellow + $object.MaxUsageMemory.toString("P")
	}
	elseif ($object.MaxUsageMemory -ge '0.9') {
		$object.MaxUsageMemory = $tag_red + $object.MaxUsageMemory.toString("P")
	}
	else{
		$object.MaxUsageMemory = $object.MaxUsageMemory.toString("P")
	}

	$array += $object
} 

##########################
#Gathering datastore data#
##########################
$array_ds = @()
get-datastore  |Where-Object {$_.name -notlike "*local*"} |ForEach-Object {
	$object = "" |Select-Object Name, Type, FreeSpaceGB,FreeSpaceRaw, Usage, ProvisionedGB,ProvisionedRaw, CapacityGB,CapacityRaw, Overcommitment
	$object.name = $_.Name
	$object.type = $_.Type
	$object.FreeSpaceGB =  [int]($_.ExtensionData.Summary.FreeSpace /1GB)
	$object.CapacityGB =  [int]($_.ExtensionData.Summary.Capacity /1GB)
	$object.FreeSpaceRaw = "N/A"
	$object.ProvisionedRaw = "N/A"
	$object.CapacityRaw = "N/A"
	$object.ProvisionedGB = [int](($_.ExtensionData.Summary.Capacity  - $_.ExtensionData.Summary.FreeSpace + $_.ExtensionData.Summary.Uncommitted)/1GB)
	
	if ($object.type -eq "vsan"){
		$object.FreeSpaceRaw = $object.FreeSpaceGB
		$object.ProvisionedRaw = $object.ProvisionedGB
		$object.CapacityRaw = $object.CapacityGB
		if (($_ |get-vmhost |Where-Object {$_.powerstate -ne "PoweredOn"}).count -ne 0) {
			$vsan_reserve="0"
		}
		else {
			$vsan_reserve = $object.CapacityGB / ($_.ExtensionData.Host.Count)
		}

		$object.FreeSpaceGB =  [int]($object.FreeSpaceGB - $vsan_reserve) / 2
		$object.CapacityGB =  [int]($object.CapacityGB - $vsan_reserve) / 2
		$object.ProvisionedGB = $object.ProvisionedGB / 2
	}
	$object.Overcommitment =  ($object.ProvisionedGB / $object.CapacityGB)
	$object.Usage = (($object.CapacityGB - $object.FreeSpaceGB) / $object.CapacityGB)
	#barvicky
	if ($object.Usage -ge '0.65' -AND $object.Usage -lt '0.85'){
		$object.Usage = $tag_yellow + $object.Usage.toString("P")
	}
	elseif ($object.Usage -ge '0.85') {
		$object.Usage = $tag_red + $object.Usage.toString("P")
	}
	else {
		$object.Usage = $object.Usage.toString("P")
	}

	if ($object.Overcommitment -ge '1' -AND $object.Overcommitment -lt '1.50'){
		$object.Overcommitment = $tag_yellow + $object.Overcommitment.toString("P")
	}
	elseif ($object.Overcommitment -ge '1.50') {
		$object.Overcommitment = $tag_red + $object.Overcommitment.toString("P")
	}
	else{
		$object.Overcommitment = $object.Overcommitment.toString("P")
	}
	$array_ds += $object
}


#############
#Create HTML#
#############
"Dear All,<br><br>This is automatically generated CapacityGB report of Softlayer clusters and hosts on weekly bases. <br><br>"  |out-file $filelocation

$report = $null
$report = $array_ov |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Basic overview</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation

$report = $null
$report = $array |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Cluster and host data</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")

$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation
$report = $null
$report = $array_ds |Sort-Object name |ConvertTo-Html -Head $style -body "<H2>Datastore data</H2>" 
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string | Out-File -Append $filelocation
"<H3>Legenda:</H3>
CPU_Overcommitment - Ratio between physical CPU and assigned virtual CPU<br>
countCPU - Physical cores without HyperThreading<br>
Memory_Overcommitment - Ratio between allocated/provisioned memory and host memory<br>
FreeSpaceGB - Data usable for virtual machines and data. For vSAN divided by 2 and minus one host reserve<br>
FreeSpaceRaw - Raw capacity visible from vcenter and by hosts. Only for vSAN datastores <br>
Datastore overcommitment - Ratio between provisioned storage to virtual machines and datastore capacity. ProvisionedGB/CapacityGB. <br>
<H3>Thresholds: </H3>
CPU usage: Warning threshold 70%. Error threshold 85% <br>
Memory usage: Warning threshold 70%. Error threshold 90% <br>
Datastore usage:  Warning threshold 65%. Error threshold 85% <br>
Datastore overcommitment: Warning threshold 100%. Error threshold 150% <br>"|out-file -FilePath $filelocation -Append
"<br>Best Regards<br><br> " |out-file -FilePath $filelocation -Append

#################
#Convert to HTML#
#################
$body = get-content $filelocation  |out-string



######################
# E-mail HTML output #
######################
if ($enablemail){
	if ($array) {
		Send-MailMessage -From $mailfrom -To $to -CC $cc -Subject $subject -SmtpServer $smtpServer -BodyAsHtml -Body $body -usessl
	}
	else
	{
		Send-MailMessage -From $mailfrom -To $to_error  -Subject "Capacity report failed" -SmtpServer $smtpServer -usessl
	}
}



##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
###########################################################################################
# Title:	Get vcenter alerts 
# Filename:	VC_getallerts.ps1     	
# Created by:	Jan Mahnig		
# Date:	1/23/2018				
# Version    1.0  					
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}
if (!(test-path C:\scripts\vmware\healthchecks\)){
	new-item -path C:\scripts\vmware\healthchecks -type Directory
}
start-transcript -path c:\scripts\vmware\logs\VC_getallerts.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue


connect-viserver $vcserver -WarningAction SilentlyContinue
$filelocation = "C:\Scripts\VMware\healthchecks\alarms.csv"






##################
#Triggered alarms#
##################
$array = @()
$alarms = (Get-Datacenter |get-view).TriggeredAlarmState
$alarms |ForEach-Object {
	$object = "" |select-object Name, Parent, Alarm, Time, Status
	$alarm = $_
	switch ($_.entity.type){
		"ClusterComputeResource" {
			$object.name = get-cluster -id $alarm.entity
			$object.Parent = "N/A"
		}
		"VirtualMachine" {
			$object.name = get-vm -id $alarm.entity
			$object.Parent = ($object.name).VMHost
		}
		"HostSystem" {
			$object.name = get-vmhost -id $alarm.entity
			$object.Parent = ($object.name).Parent
		}
		"Datastore" {
			$object.name = get-datastore -id $alarm.entity
			$object.Parent = "N/A"
		}
	}	
	switch ($_.OverallStatus){
		"yellow" {$object.Status = "Warning"}
		"red" {$object.Status = "Error"}
	}
	$object.time = $alarm.time
	$object.alarm = (get-alarmdefinition -id $alarm.alarm).name
	$array += $object
}	
$array |export-csv $filelocation -append -NoTypeInformation


##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
$list = (get-vm  |where {$_.powerstate -ne "poweredoff"} | where {$_.name -like "*.sl.dsg-i.com*" -AND ($_ | Get-HardDisk |where {$_.storageformat -ne "thin"}) -AND (($_ |get-harddisk |measure-object -sum capacitygb).sum) -le 700}).name

$list |foreach {

$vm = get-vm $_
$datastore_local = $vm |get-vmhost |get-datastore *Local_Datastore*
$datastore_vsan = $vm |get-vmhost |get-datastore *cluster*
$vm |Move-VM -Datastore $datastore_local -StorageFormat thin
$vm |Move-VM -Datastore $datastore_vsan -StorageFormat thin -runasync

}
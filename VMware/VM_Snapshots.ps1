###########################################################################################
# Title:	VM backup snapshots 
# Filename:	VM_Snapshots.ps1     	
# Created by:	Jan Mahnig		
# Date:	1/23/2018				
# Version    1.1  					
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}
start-transcript -path c:\scripts\vmware\logs\VM_Snapshots.txt

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue
connect-viserver $vcserver  -WarningAction SilentlyContinue

####################
# Define variables #
####################


$to = ""
$cc = ""
$to_error = ""
$smtpServer = "" 
$mailfrom = ""

##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "VM snapshots failed" -SmtpServer $smtpServer -usessl
	stop-transcript
	exit
}

###########
#Snapshots#
###########
get-folder |where {$_.name -like "*Windows*"  -OR $_.name -like "*basket*"} |get-vm |foreach {
	$VM = (($_ |get-view).config | where {$_.guestfullname -like "*windows*"}).name
	if ((get-vm $VM |Get-HardDisk |Measure-Object -Sum CapacityGB).sum -le 1500 -AND ((get-vm $VM).PowerState -ne "PoweredOff")) {
		$snapshot_name = (get-date -UFormat '%m%d%y')+" backup snapshot for "+$vm
		get-vm $vm |New-Snapshot -Name $snapshot_name -Memory:$false
		get-vm $vm |get-snapshot  |where {$_.created -lt ((get-date).adddays(-1)) -AND $_.name -like "*backup snapshot *"} |remove-snapshot -Confirm:$false
		(get-vm $vm).ExtensionData.ConsolidateVMDisks()
	}
}


##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
﻿###########################################################################################
# Title:	Virtual machine consumption report
# Filename:	VMlist.ps1     	
# Created by:	Jan Mahnig		
# Date:	5.6.2018					
# Version    1.8 					
###########################################################################################
if (!(test-path C:\scripts\VMware\logs\)){
	new-item -path C:\scripts\VMware\logs\ -type Directory
}
if (!(test-path C:\scripts\vmware\VMlist\)){
	new-item -path C:\scripts\vmware\VMlist -type Directory
}
start-transcript -path c:\scripts\vmware\logs\VMlist.txt
####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

##################
# Add VI-toolkit #
##################
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue


connect-viserver $vcserver -WarningAction SilentlyContinue


####################
# Define variables #
####################
$filelocation  = "C:\scripts\vmware\VMlist\vmlist_"+(get-date -format MMMM)+".html"
$subject = "Monthly VM report from " + (get-date -format MMMM)
$to = ""
$cc = ""
$to_error = ""
$smtpServer = "" 
$mailfrom = ""
######################
# Define HTML styles #
######################
$tag_red = "<span style='background-color: red'>"
$tag_yellow = "<span style='background-color: yellow'>"
$style = "<style>BODY{font-family: Arial; font-size: 9pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"


##############################
# Testing vCenter connection #
##############################
$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "VMlist failed" -SmtpServer $smtpServer -usessl
	stop-transcript
	exit
}

##################
# Gathering data #
##################

########################################
# List of VMs and resource consumption #
########################################
#nacteni seznamu VM do pole, vynechani templates, nsx a vypnutych VM
$array = @()
$vms = get-folder -type VM |where {$_.name -notlike "*nsx*" -AND $_.name -notlike "*templates*" -AND $_.name -notlike "project.use" -AND $_.name -notlike "*vm*" } |get-vm |where {$_.powerstate -eq "PoweredOn"}
$vms |Foreach {
	$view = $_ |get-view
	$object = "" | Select-Object Name, Cluster, NumCPU, MemoryGB, Disk_Size, CPU_usage, Memory_usage, Free_space, Uptime, OS
	$object.name = $_.name
	$object.cluster = ($_ |get-vmhost).parent.name
	$object.NumCPU = $_.NumCPU
	$object.MemoryGB = $_.MemoryGB
	$object.OS= $view.guest.guestfullname
	$vmguest = $_ |get-vmguest |Select-Object -ExpandProperty disks
	$object.Disk_Size = "{0:N0}" -f ((Get-HardDisk -VM $_ | Measure-Object -Sum CapacityGB).Sum)
	$stats = get-stat -Entity $_ -stat mem.usage.average,cpu.usage.average,sys.uptime.latest -start (get-date).adddays(-30) -MaxSamples 60
	$object.uptime = "{0:dd:HH:mm:ss}" -f ([datetime]([timespan]::fromseconds(($stats |where {$_.Metricid -eq "sys.uptime.latest"}| select -first 1).value)).Ticks)
	$object.Memory_usage =  ($stats |where {$_.Metricid -eq "mem.usage.average"} |Measure-Object -Property value -Average).average/100
	$object.CPU_usage = ($stats |where {$_.Metricid -eq "cpu.usage.average"} |Measure-Object -Property value -Average).average/100 
	$object.Free_space = (1 - ($vmguest | Measure-Object -Sum freespacegb).sum / ($vmguest |Measure-Object -Sum CapacityGB).sum)
	#colors
	
	if ($object.Memory_usage -ge '0.65' -AND $object.Memory_usage -lt '0.85'){
		$object.Memory_usage = $tag_yellow + ("{0:P0}" -f $object.Memory_usage)
	}
	elseif ($object.Memory_usage -ge '0.85'){
		$object.Memory_usage = $tag_red + ("{0:P0}" -f $object.Memory_usage)
	}
	else {
		$object.Memory_usage = "{0:P0}" -f $object.Memory_usage
	}
	
	if ($object.CPU_usage -ge '0.65' -AND $object.CPU_usage -lt '0.85'){
		$object.CPU_usage = $tag_yellow + ("{0:P0}" -f $object.CPU_usage)
	}
	elseif ($object.CPU_usage -ge '0.85'){
		$object.CPU_usage = $tag_red + ("{0:P0}" -f $object.CPU_usage)
	}
	else {
		$object.CPU_usage = "{0:P0}" -f $object.CPU_usage
	}	
	
	if ($object.Free_space -ge '0.80' -AND $object.Free_space -lt '0.90'){
		$object.Free_space = $tag_yellow + ("{0:P0}" -f $object.Free_space)
	}
	elseif ($object.Free_space -ge '0.90'){
		$object.Free_space = $tag_red + ("{0:P0}" -f $object.Free_space)
	}
	else {
		$object.Free_space = "{0:P0}" -f $object.Free_space
	}
	$array += $object
}

#########################
#New VMs from last month#
#########################
$newVMs = Get-VIEvent -maxsamples ([int]::MaxValue) -Start (Get-Date).AddDays(–30) | where {$_.Gettype().Name-eq "VmCreatedEvent" -or $_.Gettype().Name-eq "VmBeingClonedEvent" -or $_.Gettype().Name-eq "VmBeingDeployedEvent"} |where {$_.username -ne "com.vmware.vsan.health"} 
$array_NewVM = @()
$newVMs |ForEach-Object {
	$object = "" | Select-Object CreatedTime, UserName, VM
	$object.CreatedTime = $_.CreatedTime
	$object.username = $_.username
	if ($_.FullformattedMessage -like "Cloning*") {	
		$object.VM = $_.FullformattedMessage -split "Dixons_Carphone to" | Select-Object -Last 1
		$object.VM = $object.VM -split "on host" | Select-Object -First 1
		$object.VM  = $object.VM.Trim(" ")  
	}	
	else
	{
		$object.VM = $_.vm.name	
	}
	if (get-vm $object.vm){		
		$array_NewVM += $object	
	}
}
	
	
#$delVMs = Get-VIEvent -maxsamples ([int]::MaxValue) -Start (Get-Date).AddDays(–30) | where {$_.Gettype().Name-eq "VmRemovedEvent"} |where {$_.username -ne "com.vmware.vsan.health"} 	
#$array_DelVM = @()
#$delVMs  | foreach {
#	$object = "" | Select-Object DeletedTime, UserName, VM
#	$object.username = $_.username
#	$object.DeletedTime = $_.CreatedTime
#	$object.VM = $_.vm.name
#	$array_DelVM += $object
#}


#################
#Convert to HTML#
#################
#konvert z array na html string, pridani stylu a head
$precontent = "Total " + $array_NewVM.count + " servers created last month"
$report = $array_NewVM  |Sort-Object Cluster, Name |ConvertTo-html -Head $style -Body "<br>This is automatically generated VM report of Softlayer virtual machines on monthly bases. <br><br><br><br>" -postcontent "<br><br><br>" -precontent $precontent
$report = $report +($array |convertto-html -precontent "Server configuration and resource consumption" )
$report = $report.replace("&lt;","<")
$report = $report.replace("&gt;",">")
$report = $report.replace("&quot;","'")
$report = $report.replace("><span","")
$report = $report.replace("&#39;","'") |out-string
#export do souboru pro archivovani
$report  |out-file -filepath $filelocation 


######################
# E-mail HTML output #
######################
#check jestli report obsahuje nejaka data, zabraneni odeslani prazdneho emailu, reporting v pripade prazdneho reportu
if ($array -AND $array_NewVM -AND $report) {
#odeslani reportu
	Send-MailMessage -From $mailfrom -To $to -cc $cc -Subject $subject -SmtpServer $smtpServer -BodyAsHtml -Body $report -usessl
}
else
{
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "VMlist failed" -SmtpServer $smtpServer -usessl
}


##############################
# Disconnect session from VC #
##############################
stop-transcript
disconnect-viserver -confirm:$false

#################
# End Of script #
#################
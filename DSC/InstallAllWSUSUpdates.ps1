Configuration InstallAllWSUSUpdates
{

    param
    (
    [Parameter(Mandatory=$true)]
    [string[]]$ComputerName
    )

    Import-DscResource -ModuleName "PSDesiredStateConfiguration"
	node $ComputerName 
    {
		LocalConfigurationManager
        {
            RebootNodeIfNeeded = $true
		}
		
        Registry 'NoAUShutdownOption'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'NoAUShutdownOption'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'DetectionFrequencyEnabled'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'DetectionFrequencyEnabled'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'DetectionFrequency'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'DetectionFrequency'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'UseWUServer'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'UseWUServer'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'AutoInstallMinorUpdates'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'AutoInstallMinorUpdates'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'NoAutoRebootWithLoggedOnUsers'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
            ValueName = 'NoAutoRebootWithLoggedOnUsers'
            ValueType = 'DWord'
            ValueData = '1'
        }
        Registry 'RebootRelaunchTimeoutEnabled'
        {
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'RebootRelaunchTimeoutEnabled'
			ValueType = 'DWord'
			ValueData = '1'
		}
		Registry 'RebootRelaunchTimeout'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'RebootRelaunchTimeout'
			ValueType = 'DWord'
			ValueData = '1440'
		}
		Registry 'NoAutoUpdate'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'NoAutoUpdate'
			ValueType = 'DWord'
			ValueData = '0'
		}
		Registry 'AUOptions'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'AUOptions'
			ValueType = 'DWord'
			ValueData = '2'
			}
		Registry 'AutomaticMaintenanceEnabled'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'AutomaticMaintenanceEnabled'
			ValueType = 'String'
			ValueData = ' '
		}
		Registry 'ScheduledInstallDay'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'ScheduledInstallDay'
			ValueType = 'DWord'
			ValueData = '0'
		}
		Registry 'ScheduledInstallTime'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate\AU'
			ValueName = 'ScheduledInstallTime'
			ValueType = 'DWord'
			ValueData = '20'
		}
		Registry 'DisableOSUpgrade'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate'
			ValueName = 'DisableOSUpgrade'
			ValueType = 'DWord'
			ValueData = '1'
		}
		Registry 'TargetGroupEnabled'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate'
			ValueName = 'TargetGroupEnabled'
			ValueType = 'DWord'
			ValueData = '1'
		}
		Registry 'TargetGroup'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate'
			ValueName = 'TargetGroup'
			ValueType = 'String'
			ValueData = 'Nonprod Servers'
		}
		Registry 'WUServer'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate'
			ValueName = 'WUServer'
			ValueType = 'String'
			ValueData = 'http://10.43.3.3:8530'
		}
		Registry 'WUStatusServer'
		{
			Ensure = 'Present'
			Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\WindowsUpdate'
			ValueName = 'WUStatusServer'
			ValueType = 'String'
			ValueData = 'http://10.43.3.3:8530'
		}

	    Service WindowsUpdate
		{
			Name ="wuauserv"
			StartupType="Automatic"
			}

		Script InstallUpdates            
		{            
			GetScript = {
				$Criteria = "IsInstalled=0 and Type='Software'"
				$Searcher = New-Object -ComObject Microsoft.Update.Searcher
				$SearchResult = $Searcher.Search($Criteria).Updates     
				Write-Verbose 'Number of patches to install is $SearchResult.Count'
				$NumberOfPatches = $SearchResult.Count
                Write-Verbose "Currently there are $NumberOfPatches patches to install."
				Return @{            
					'Result' = "Currently there are $NumberOfPatches patches to install."            
				}            
			}            
			TestScript = {    
				$Criteria = "IsInstalled=0 and Type='Software'"
				$Searcher = New-Object -ComObject Microsoft.Update.Searcher
				$SearchResult = $Searcher.Search($Criteria).Updates

				#$SearchResult = (New-Object -ComObject Microsoft.Update.Session).CreateUpdateSearcher();$searcher.Search($Criteria).Updates


				If ($SearchResult.count -eq 0) {
					Write-Verbose 'No patches waiting to install'
					Return $true
				}
				else {
					Write-Verbose 'Patches are still waiting to install'
					Return $false
				}      
			}            
         
			SetScript = {     
				$Criteria = "IsInstalled=0 and Type='Software'"
				$Searcher = New-Object -ComObject Microsoft.Update.Searcher
				$SearchResult = $Searcher.Search($Criteria).Updates
				$Session = New-Object -ComObject Microsoft.Update.Session
				$Downloader = $Session.CreateUpdateDownloader()
				$Downloader.Updates = $SearchResult
				$Downloader.Download()
				$Installer = New-Object -ComObject Microsoft.Update.Installer
				$Installer.Updates = $SearchResult
				$Result = $Installer.Install()
				If ($Result.rebootRequired) { 
					$global:DSCMachineStatus = 1
				}           
			}
            DependsOn = "[Service]WindowsUpdate"
		}

	}
}
cd D:\DSC_configuration
InstallAllWSUSUpdates -computername GBSLVDWTEST05.dsg.dsgroot.int

#(get-content D:\DSC\hostname.txt)
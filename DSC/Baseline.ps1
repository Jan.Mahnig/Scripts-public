#pull server configuration
configuration Baseline
{
    param
    (
    [Parameter(Mandatory=$false)]
    [string[]]$ComputerName,
    [PSCredential]$DomainCred
    )
    Import-DscResource -ModuleName PSDesiredStateConfiguration 
    Import-DscResource -ModuleName SecurityPolicyDsc
    Import-DscResource -ModuleName AuditPolicyDsc
    Import-DscResource -ModuleName xTimeZone
    Import-DscResource -ModuleName xSystemSecurity
    Import-DscResource -ModuleName xRemoteDesktopAdmin
    Import-DscResource -ModuleName xComputerManagement
    Import-DscResource -ModuleName xNetworking
    Import-DscResource -ModuleName cChoco

    $source = "\\hostname\DSCsources$\"
    node "localhost"
    {
        LocalConfigurationManager {
            ConfigurationModeFrequencyMins = 15
            ConfigurationMode = "ApplyAndAutocorrect"
            RebootNodeIfNeeded = $true 
        } 
        

##################################################
#################Security config##################
##################################################


        AccountPolicy SecPol_AccountPolicy
        {
            Name = "SecPol_AccountPolicy"
            Minimum_Password_Age = 1
            Maximum_Password_Age = 90
            Enforce_password_history = 12
            Minimum_Password_Length = 8
            Password_must_meet_complexity_requirements = "Disabled"
            Account_lockout_threshold = 3
            Account_lockout_duration = 30
            Reset_account_lockout_counter_after = 30
        } 

        UserRightsAssignment "ByeBye IBM"
        {
            Policy = "Deny_log_on_through_Remote_Desktop_Services"
            Ensure = "Present"
            Identity = "DSG\IBM-ROLE-SVR-G-ADMIN"

        }


        SecurityOption "don't display username"
        {
            Name = "Interactive_logon_Do_not_display_last_user_name"
            Interactive_logon_Do_not_display_last_user_name = 'enabled'
        }

        SecurityOption "DC logon cache"
        {
            Name = "DC logon cache"
            Interactive_logon_Number_of_previous_logons_to_cache_in_case_domain_controller_is_not_available = 10
        }

        SecurityOption CTRLALTDEL
        {
            Name = "CTRLALTDEL"
            Interactive_logon_Do_not_require_CTRL_ALT_DEL = "Disabled"
        }

##################################################
#####enable TLS1.2 and disable SSL3.0 and SMB1####
##################################################
        Registry TLS1_2ServerEnabled
        {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'Enabled'
            ValueData = 1
            ValueType = 'Dword'
        }
        Registry TLS1_2ServerDisabledByDefault
        {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server'
            ValueName = 'DisabledByDefault'
            ValueData = 0
            ValueType = 'Dword'
        }
        Registry NumlockOnStartup
        {
            Ensure = 'Present'
            Key = 'HKU:\.Default\Control Panel\Keyboard'
            ValueName = 'InitialKeyboardIndicators '
            ValueData = 2
            ValueType = 'Dword'
        }
        Registry TLS1_2ClientEnabled
        {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'Enabled'
            ValueData = 1
            ValueType = 'Dword'
        }
        Registry TLS1_2ClientDisabledByDefault
        {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client'
            ValueName = 'DisabledByDefault'
            ValueData = 0
            ValueType = 'Dword'
        }
        Registry SSL2ServerDisabled
        {
            Ensure = 'Present'
            Key = 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\SSL 2.0\Server'
            ValueName = 'Enabled'
            ValueData = 0
            ValueType = 'Dword'
        }

###################
#####RDP config####
###################
        xRemoteDesktopAdmin RemoteDesktopSettings
        {
           Ensure = 'Present'
           UserAuthentication = 'Secure'
        }
        
        Registry LegalNoticeCaption
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
            ValueName = 'LegalNoticeCaption'
            ValueData = "*****WARNING*****"
            ValueType = 'String'
            Force = $true
       }    
       
        Registry LegalNoticeText
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
            ValueData = 'This computer system is the property of Dixons Carphone, contains confidential information, and may be accessed only by authorised users. You may use the system only for purposes authorised by Dixons Carphone and in accordance with Dixons Security Policy, and you may not download, delete or change any data on the system without Dixons Carphone permission. Unauthorised use of this system, including any attempt to conceal activity, is strictly prohibited and may be subject to disciplinary action and / or civil or criminal prosecution. Dixons Carphone may monitor any activity or communication on the system and retrieve any information stored within the system. By accessing and using this computer, you are consenting to such monitoring and information retrieval for law enforcement and other purposes. Users should have no expectation of privacy as to any communication on or information stored within the system, including information stored locally on the system or any attached media. By proceeding, you acknowledge that you (1) have read and understand this notice, and (2) consent to the system monitoring.'
            ValueName = 'LegalNoticeText'
            ValueType = 'String'
            Force = $true
       } 

########################
#auditing configuration#
########################        
        #Force Advanced Audit Policy 
        Registry SCENoApplyLegacyAuditPolicy
        {
            Ensure = 'Present'
            Key = "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa"
            ValueName = 'SCENoApplyLegacyAuditPolicy'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        } 

        AuditPolicySubcategory "Credential Validation"{
            Name      = 'Credential Validation'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Kerberos Authentication Service"{
            Name      = 'Kerberos Authentication Service'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Kerberos Service Ticket Operations"{
            Name      = 'Kerberos Service Ticket Operations'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other Account Logon Events"{
            Name      = 'Other Account Logon Events'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Application Group Management"{
            Name      = 'Application Group Management'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Computer Account Management"{
            Name      = 'Computer Account Management'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Distribution Group Management"{
            Name      = 'Distribution Group Management'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other Account Management Events"{
            Name      = 'Other Account Management Events'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "User Account Management"{
            Name      = 'User Account Management'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Detailed Directory Service Replication"{
            Name      = 'Detailed Directory Service Replication'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Directory Service Access"{
            Name      = 'Directory Service Access'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Directory Service Changes"{
            Name      = 'Directory Service Changes'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
        AuditPolicySubcategory "Directory Service Replication"{
            Name      = 'Directory Service Replication'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Account Lockout"{
            Name      = 'Account Lockout'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "User / Device Claims"{
            Name      = 'User / Device Claims'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "IPsec Extended Mode"{
            Name      = 'IPsec Extended Mode'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "IPsec Main Mode"{
            Name      = 'IPsec Main Mode'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "IPsec Quick Mode"{
            Name      = 'IPsec Quick Mode'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Logoff"{
            Name      = 'Logoff'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Logon"{
            Name      = 'Logon'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Network Policy Server"{
            Name      = 'Network Policy Server'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Other Logon/Logoff Events"{
            Name      = 'Other Logon/Logoff Events'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Special Logon"{
            Name      = 'Special Logon'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Application Generated"{
            Name      = 'Application Generated'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Certification Services"{
            Name      = 'Certification Services'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }		
		AuditPolicySubcategory "Detailed File Share"{
            Name      = 'Detailed File Share'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "File Share"{
            Name      = 'File Share'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
        AuditPolicySubcategory "File System"{
            Name      = 'File System'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		
		AuditPolicySubcategory "Filtering Platform Connection"{
            Name      = 'Filtering Platform Connection'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Filtering Platform Packet Drop"{
            Name      = 'Filtering Platform Packet Drop'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Handle Manipulation"{
            Name      = 'Handle Manipulation'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Kernel Object"{
            Name      = 'Kernel Object'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other Object Access Events"{
            Name      = 'Other Object Access Events'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Registry"{
            Name      = 'Registry'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Removable Storage"{
            Name      = 'Removable Storage'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "SAM"{
            Name      = 'SAM'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Central Policy Staging"{
            Name      = 'Central Policy Staging'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Audit Policy Change"{
            Name      = 'Audit Policy Change'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Authentication Policy Change,"{
            Name      = 'Authentication Policy Change'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Authorization Policy Change"{
            Name      = 'Authorization Policy Change'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Filtering Platform Policy Change"{
            Name      = 'Filtering Platform Policy Change'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "MPSSVC Rule-Level Policy Change"{
            Name      = 'MPSSVC Rule-Level Policy Change'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other Policy Change Events"{
            Name      = 'Other Policy Change Events'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Non Sensitive Privilege Use"{
            Name      = 'Non Sensitive Privilege Use'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other Privilege Use Events"{
            Name      = 'Other Privilege Use Events'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Sensitive Privilege Use"{
            Name      = 'Sensitive Privilege Use'
            AuditFlag = 'Success'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "IPsec Driver"{
            Name      = 'IPsec Driver'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Other System Events"{
            Name      = 'Other System Events'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "System Integrity"{
            Name      = 'System Integrity'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Security System Extension"{
            Name      = 'Security System Extension'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
		AuditPolicySubcategory "Security State Change"{
            Name      = 'Security State Change'
            AuditFlag = 'Failure'
            Ensure    = 'Present'
        }
        
        #disable customer experience imp program
        Registry CEIPEnable 
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\SQMClient\Windows"
            ValueName = 'CEIPEnable'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 

        #optimize priority for background services
        Registry Win32PrioritySeparation 
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\PriorityControl"
            ValueName = 'Win32PrioritySeparation'
            ValueData = 24
            ValueType = 'Dword'
            Force = $true
        } 

        Registry VisualFXSetting 
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects"
            ValueName = 'VisualFXSetting'
            ValueData = 2
            ValueType = 'Dword'
            Force = $true
        } 

        Registry RestrictGuestAccessApp 
        {
            Ensure = 'Present'
            Key = "HKLM:\SYSTEM\CurrentControlSet\Services\Eventlog\Application"
            ValueName = 'RestrictGuestAccess'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        }
        Registry RestrictGuestAccessSys 
        {
            Ensure = 'Present'
            Key = "HKLM:\SYSTEM\CurrentControlSet\Services\Eventlog\System"
            ValueName = 'RestrictGuestAccess'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        }
        Registry RestrictGuestAccessSec
        {
            Ensure = 'Present'
            Key = "HKLM:\SYSTEM\CurrentControlSet\Services\Eventlog\Security"
            ValueName = 'RestrictGuestAccess'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        }
        Registry RestrictGuestAccessDNS
        {
            Ensure = 'Present'
            Key = "HKLM:\SYSTEM\CurrentControlSet\Services\Eventlog\DNS server"
            ValueName = 'RestrictGuestAccess'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        }

        #disable IE enhanced security for admins
        xIEEsc DisableIEEs
        {
            UserRole = "administrators"
            IsEnabled = $false
        }

        xTimeZone Timezone
        {
            TimeZone = "GMT Standard Time"
            IsSingleInstance = 'Yes'   
        }

        Script DisableFirewall 
        {
            GetScript = {
                @{
                    Result = -not('True' -in (Get-NetFirewallProfile -All).Enabled)
                }
             }
        
            SetScript = {
                Set-NetFirewallProfile -All -Enabled False -Verbose
            }

            TestScript = {
                $Status = -not('True' -in (Get-NetFirewallProfile -All).Enabled)
                $Status -eq $True
            }
        }

        Script ChangeCDROMDriveLetter 
        {
            GetScript = {
                @{
                    Result = (Get-CimInstance -ClassName Win32_Volume -Filter "DriveLetter = 'z:'").DriveType -eq 5
                }           
            }            
            SetScript = {
                Get-CimInstance -ClassName Win32_Volume -Filter "DriveLetter = 'D:'" | Set-CimInstance -Property @{ DriveLetter = "Z:" }
            }
            TestScript = {
                $Status = (Get-CimInstance -ClassName Win32_Volume -Filter "DriveLetter = 'Z:'").DriveType -eq 5
                $Status -eq $True
            }
        }

        Script BCDeditTimeout
        {
        TestScript = {		
				if ((bcdedit |findstr timeout) -notlike "*5*"){     
                    #config doesn't match, start "setscript"
					Return $false
				}
				else {
                    #config match, do nothing
					Return $true
				}
			}
            SetScript = {
                bcdedit /timeout 5
	            }
            GetScript = {
            @{
                Result = (bcdedit |findstr timeout)
                }
            }
       
        }

        Script "Disable hibernation"
        {
        TestScript = {		
				if ((Get-ItemProperty HKLM:\SYSTEM\CurrentControlSet\Control\Power -name HibernateEnabled).HibernateEnabled -eq 1){     
                    #config doesn't match, start "setscript"
					Return $false
				}
				else {
                    #config match, do nothing
					Return $true
				}
			}
            SetScript = {
                Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Control\Power -Name HibernateEnabled -Value 0 -Force -Confirm:$false
                powercfg -h off
	            }
            GetScript = {
            @{
                Result = (Get-ItemProperty HKLM:\SYSTEM\CurrentControlSet\Control\Power -name HibernateEnabled)
                }
            }
       
        }

        WindowsFeature Telnet-Client
        {
           Ensure = "Present"
           Name   = "Telnet-Client"
        }
        
        WindowsFeature Telnet-Server
        {
           Ensure = "Absent"
           Name   = "Telnet-Server"
        }

        WindowsFeature Simple-TCPIP
        {
           Ensure = "Absent"
           Name   = "Simple-TCPIP"
        }
        
        WindowsFeature NET-Framework-Core
        {
           Ensure = "Present"
           Name   = "NET-Framework-Core"
           Source = "$source\sxs"
        }
        
        WindowsFeature SMB1
        {
            Ensure = "Absent"
            Name = "FS-SMB1"
        }

        File Tools_folder
        {
            Ensure          = "Present"
            SourcePath      = "$source\tools"
            DestinationPath = "c:\tools\"
            Type            = "Directory"
            Recurse = $true
            Force =             $true
            Checksum ='ModifiedDate' 
            PsDscRunAsCredential = $DomainCred
            MatchSource = $true
        }            
               
        Registry NoDriveTypeAutorun
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"
            ValueName = 'NoDriveTypeAutorun'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 
        
        #disable UAC
        Registry EnableLUA
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\policies\system"
            ValueName = 'EnableLUA'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 
        

        Registry 'UserAuthentication'
        {
            Ensure = 'Present'
            Key = 'HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows NT\Terminal Services'
            ValueName = 'UserAuthentication'
            ValueType = 'DWord'
            ValueData = '0'
            Force = $true
        }
     
        #File "Delete LMCConfig"
        #{
            #Ensure          = "present"
            #DestinationPath = "c:\LCMConfig.ps1"
            #Type            = "File"
        # 


        File Test-file
        {
            Ensure          = "Present"
            DestinationPath = "c:\testDSC.txt"
            Type            = "File"
            Contents = "test DSC pull"
        } 


        xPowerPlan SetPlanHighPerformance
        {
            IsSingleInstance = 'Yes'
            Name             = 'High performance'
        }
#############
#Domain join#
#############
        xComputer JoinDomain
        {
            DomainName = "dsg.dsgroot.int"
            JoinOU = "OU=Tools,OU=SoftLayer,OU=Servers,OU=DCG,OU=Non-Production,DC=DSG,DC=DSGROOT,DC=INT"
            Name = 'localhost'
            Credential = $DomainCred
        }

        Groupset LocalAdmins
        {
            GroupName        = 'Administrators'
            MembersToInclude = 'DSG\DCG-ROLE-G-BRNO_WIN-ADMINS'
            #MembersToExclude = 'DSG\IBM-ROLE-SVR-G-ADMIN'
            Credential       = $DomainCred
            Ensure = 'Present'
            DependsOn = '[xComputer]JoinDomain'
        }


##################################################
######################Tooling#####################
##################################################

        Environment proxy
        {
            ensure = "present"
            Name = "chocolateyProxyLocation"
            Value = "10.43.3.21:3128"
        }

        service windowsupdate
        {
            Name = "wuauserv"
            State = 'Running'
            StartupType = 'Automatic'
        }

        cChocoInstaller installChoco
        {
            InstallDir = "c:\ProgramData\chocolatey"
            ChocoInstallScriptUrl = "\\GBSLVDWSDSC001.dsg.dsgroot.int\dsc\sources\Chocoinstall.ps1"
        }

        cChocoPackageInstaller powershell
        {
            Name                 = 'powershell'
            Ensure               = 'Present'
            DependsOn            = '[cChocoInstaller]installChoco','[Service]windowsupdate'
            AutoUpgrade = $true 
            chocoParams = "--proxy='10.43.3.21:3128'"
        }


        cChocoPackageInstaller Naemon
        {              
            Name                 = 'nscp'
            Ensure               = 'Present'
            DependsOn            = '[cChocoInstaller]installChoco' 
            AutoUpgrade = $false
            #Version = "5.1.44"
            chocoParams = "--proxy='10.43.3.21:3128'"    
        }

        File Naemon_scripts
        {
            Ensure          = "Present"
            SourcePath      = "$source\Naemon\NSClient++\scripts"
            DestinationPath = "C:\Program Files\NSClient++\scripts"
            Type = "Directory"
            Recurse = $true
            Force = $true
            MatchSource = $true
            Checksum = "modifiedDate"
            #DependsOn = '[Package]Naemon_install'
            DependsOn = '[cChocoPackageInstaller]Naemon'
        }    
                #Package Naemon_install
        #{
            #Ensure ="Present"
            #Path ="$source\Naemon\NSCP-0.5.1.44-x64.msi"
            #Name ="NSClient++ (x64)"
            #ProductId="C929B934-2578-48A9-9BE2-91EF8C0D35E2"
            #DependsOn = '[xComputer]JoinDomain'
        #}
        File Naemon_config
        {
            Ensure          = "Present"
            SourcePath      = "$source\Naemon\NSClient++\nsclient.ini"
            DestinationPath = "C:\Program Files\NSClient++\nsclient.ini"
            Checksum = "modifiedDate"
            Type            = "File"
            Force = $true
            MatchSource = $true
            #DependsOn = '[Package]Naemon_install'
            DependsOn = '[cChocoPackageInstaller]Naemon'
        }  

        Service NSCP
        {
            Name ="nscp"
            StartupType="Automatic"
            State = 'Running'
            #DependsOn = '[Package]Naemon_install'
            DependsOn = '[cChocoPackageInstaller]Naemon'
        }

        Package mcAfee
        {
            Ensure ="present"
            Path ="$source\VSE\SLAVAgent504FramePkg.exe"
            Name ="McAfee Agent"
            ProductId="2B4B02CD-CA9E-4024-9B9B-2EA9950EEC11"
            DependsOn = '[xComputer]JoinDomain'

        }

        xHostsFile Networker
        {
            Ensure = 'Present'
            HostName = "bck02.sl.dsgroot.int"
            IPAddress = "10.43.3.15"
        }

        Package Networker
        {
            Ensure ="present"
            Path ="$source\Networker\lgtoclnt-9.1.0.2.exe"
            Name ="NetWorker"
            ProductId="3CABFC14-32A8-409A-AC4A-BE45517ED301"
            Arguments ='/s /q'
           DependsOn = '[xHostsFile]Networker'
        }

        Package Networker_module_for_microsoft
        {
            Ensure ="present"
            Path ="$source\Networker\NWVSS.exe"
            Name ="NetWorker Module for Microsoft"
            ProductId="8F0217EA-3E16-48B8-AA05-D96B9D26746F"
            Arguments ='/s /q /log "C:\windows\temp\NMM_nw_install_detail.log"'
           DependsOn = '[Package]Networker'
        }

        Service nsrnmmra
        {
            Name ="nsrnmmra"
            StartupType="Automatic"
            State = 'Running'
            DependsOn = '[Package]Networker'
        }

        Service nsrexecd
        {
            Name ="nsrexecd"
            StartupType="Automatic"
            State = 'Running'
            DependsOn = '[Package]Networker'
        }
        
        Registry networker_config 
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Legato\NetWorker"
            ValueName = 'Default server'
            ValueData = "bck02.sl.dsgroot.int"
            ValueType = 'String'
            Force = $true
            DependsOn = '[Package]Networker'
        } 


    }
}

$ConfigurationData  = @{
    AllNodes = @(
        @{
            NodeName = 'localhost'
            PSDscAllowPlainTextPassword = $true
            PSDscAllowDomainUser = $true
        }
    )
 }

$name="d11ccdeb-a854-4b64-abd3-4e62d8e3931e.mof"
$credential = Get-Credential

#get-content D:\DSC\hostname-pull.txt
Remove-Item D:\DSC_configuration\Baseline\*
Set-Location D:\DSC_configuration
Baseline -domaincred $credential -ConfigurationData $ConfigurationData  
#$name = (dir D:\WindowsPowerShell\DscService\Configuration *.mof).name
Remove-Item D:\WindowsPowerShell\DscService\Configuration\*
Get-ChildItem D:\DSC_configuration\Baseline\ localhost.mof |Select-Object -first 1 |Copy-Item -destination D:\WindowsPowerShell\DscService\Configuration
Get-ChildItem D:\WindowsPowerShell\DscService\Configuration\*.mof |rename-item -NewName $name
New-DscChecksum D:\WindowsPowerShell\DscService\Configuration
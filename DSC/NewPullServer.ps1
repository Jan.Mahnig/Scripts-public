﻿configuration NewPullServer
{

    Import-DscResource –ModuleName 'PSDesiredStateConfiguration'
    Import-DSCResource -ModuleName 'xPSDesiredStateConfiguration'

    Node localhost
    {
		WindowsFeature DSCServiceFeature
		{
            Ensure = “Present”
            Name   = “DSC-Service”
        }

        xDscWebService PSDSCPullServer
        {
                Ensure                  = “Present”
                EndpointName            = “PSDSCPullServer”
                Port                    = 80
                PhysicalPath            = "$env:SystemDrive\inetpub\wwwroot\PSDSCPullServer”
                CertificateThumbPrint   = “AllowUnencryptedTraffic”
                ModulePath              = "$env:PROGRAMFILES\WindowsPowerShell\Modules"
                ConfigurationPath       = “d:\WindowsPowerShell\DscService\Configuration”
                State                   = “Started”
                UseSecurityBestPractices= $false
                DependsOn               = “[WindowsFeature]DSCServiceFeature”
                RegistrationKeyPath = "D:\WindowsPowerShell\DscService\Configuration"

		}

        #xDscWebService PSDSCComplianceServer
		#{
			#Ensure                  = “Present”
			#EndpointName            = “PSDSCComplianceServer”
			#Port                    = 81
			#PhysicalPath            = "$env:SystemDrive\inetpub\wwwroot\PSDSCComplianceServer”
			#CertificateThumbPrint   = “AllowUnencryptedTraffic”
			#State                   = “Started”
			#IsComplianceServer     = $true
            #UseSecurityBestPractices= $false
			#DependsOn               = (“[WindowsFeature]DSCServiceFeature”,”[xDSCWebService]PSDSCPullServer”)
		#}
	}
}

cd D:\DSC_configuration
NewPullServer
﻿$DscResourceKit = "\\hostname\PSModules$"
Configuration copyDSCresources
{
    param
    (
            [Parameter(Mandatory=$false)]
            [string[]]$ComputerName
    ) 
    Node $ComputerName
    {
        File DscResourceKit
        {
            Ensure = 'Present'
            SourcePath = $DscResourceKit
            DestinationPath = "$env:ProgramFiles\WindowsPowerShell\Modules"
            Type = "Directory"
            Recurse = $true
            Force = $true
            Checksum = 'ModifiedDate'
            MatchSource = $true

        }

    }
}
 
copyDSCresources -computername (get-content D:\DSC\hostname.txt)
Start-DscConfiguration -Path .\copyDSCresources -Wait -force -verbose
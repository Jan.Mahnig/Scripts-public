﻿Configuration LCMConfig
{

    param
    (
    [Parameter(Mandatory=$true)]
    [string[]]$ComputerName
    )

    node $ComputerName
     {
         LocalConfigurationManager
         {
            RebootNodeIfNeeded = $true
            AllowModuleOverwrite = $true
            ConfigurationMode = "ApplyAndAutoCorrect"
            ConfigurationModeFrequencyMins = 15
            ConfigurationID = "d11ccdeb-a854-4b64-abd3-4e62d8e3931e"
            RefreshMode = 'Pull'
            DownloadManagerName = 'WebDownloadManager'
            
            DownloadManagerCustomData = @{
                ServerUrl = 'http://pullserver/PSDSCPullServer.svc'
                AllowUnsecureConnection = 'true' 
                AllowModuleOverwrite = 'True' 
            }
        } 
        
   
    }     
}
$computername = (get-content D:\DSC\hostname-pull.txt)
cd D:\DSC_configuration
LCMConfig -ComputerName $computername
Set-DscLocalConfigurationManager -Path .\LCMConfig -Verbose -force

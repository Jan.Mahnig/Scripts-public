﻿Configuration LCMConfig
{
    node localhost
     {
         LocalConfigurationManager
         {
            RebootNodeIfNeeded = $true
            AllowModuleOverwrite = $true
            ConfigurationMode = "ApplyAndAutoCorrect"
            ConfigurationModeFrequencyMins = 30
            ConfigurationID = "d11ccdeb-a854-4b64-abd3-4e62d8e3931e"
            RefreshMode = 'Pull'
            DownloadManagerName = 'WebDownloadManager'
            
            DownloadManagerCustomData = @{
                ServerUrl = 'http://dsc/PSDSCPullServer.svc'
                AllowUnsecureConnection = 'true' 
                AllowModuleOverwrite = 'True' 
            }
        } 
        
   
    }     
}

LCMConfig
Set-DscLocalConfigurationManager -Path .\LCMConfig
Update-DscConfiguration

$Providers = Get-WinEvent -ListLog *DSC*, *DesiredState* -Force |            
    Select-Object -Unique -ExpandProperty OwningProviderName            
$DSCEvents = ForEach ($Provider in $Providers) {            
    (Get-WinEvent -ListProvider $Provider).Events |            
        Select-Object `
            @{Name='Log'       ;Expression={$_.LogLink.LogName}},   `
            @{Name='LevelValue';Expression={$_.Level.Value}},       `
            @{Name='LevelName' ;Expression={$_.Level.DisplayName}}, `
            Id, Description, Template            
}            
$DSCEvents | Out-GridView -Title 'DSC Events'
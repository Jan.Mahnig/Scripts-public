Configuration Tooling
{
    $source = "\\dsc\DSCsources$\"
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    Import-DscResource -ModuleName xNetworking 
    node (get-content D:\DSC\hostname.txt)

    {     
        Package Naemon_install
        {
            Ensure ="Present"
            Path ="$source\Naemon\NSCP-0.5.1.44-x64.msi"
            Name ="NSClient++ (x64)"
            ProductId="C929B934-2578-48A9-9BE2-91EF8C0D35E2"
        }
        
        File Naemon_scripts
        {
            Ensure          = "Present"
            SourcePath      = "$source\Naemon\NSClient++\scripts"
            DestinationPath = "C:\Program Files\NSClient++\scripts"
            Type = "Directory"
            Recurse = $true
            Force = $true
            MatchSource = $true
            Checksum = "modifiedDate"
            DependsOn = '[Package]Naemon_install'
        }    
        
        File Naemon_config
        {
            Ensure          = "Present"
            SourcePath      = "$source\Naemon\NSClient++\nsclient.ini"
            DestinationPath = "C:\Program Files\NSClient++\nsclient.ini"
            Checksum = "modifiedDate"
            Type            = "File"
            Force = $true
            MatchSource = $true
            DependsOn = '[Package]Naemon_install'
        }  

        Service NSCP
        {
            Name ="nscp"
            StartupType="Automatic"
            State = 'Running'
            DependsOn = '[Package]Naemon_install'
        }

        Package mcAfee
        {
            Ensure ="present"
            Path ="$source\VSE\SLAVAgent504FramePkg.exe"
            Name ="McAfee Agent"
            ProductId="2B4B02CD-CA9E-4024-9B9B-2EA9950EEC11"

        }

        xHostsFile Networker
        {
            Ensure = 'Present'
            HostName = "bck02.sl.dsgroot.int"
            IPAddress = "10.43.3.15"
        }


        Package Networker
        {
            Ensure ="present"
            Path ="$source\Networker\lgtoclnt-9.1.0.2.exe"
            Name ="NetWorker"
            ProductId="3CABFC14-32A8-409A-AC4A-BE45517ED301"
            Arguments ='/s /q'
           DependsOn = '[xHostsFile]Networker'
        }



        Package Networker_module_for_microsoft
        {
            Ensure ="present"
            Path ="$source\Networker\NWVSS.exe"
            Name ="NetWorker Module for Microsoft"
            ProductId="8F0217EA-3E16-48B8-AA05-D96B9D26746F"
            Arguments ='/s /q /log "C:\windows\temp\NMM_nw_install_detail.log"'
           DependsOn = '[xHostsFile]Networker','[Package]Networker'
        }

        Service nsrnmmra
        {
            Name ="nsrnmmra"
            StartupType="Automatic"
            State = 'Running'
            DependsOn = '[Package]Networker'
        }

        Service nsrexecd
        {
            Name ="nsrexecd"
            StartupType="Automatic"
            State = 'Running'
            DependsOn = '[Package]Networker'
        }
        

        
        Registry networker_config 
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Legato\NetWorker"
            ValueName = 'Default server'
            ValueData = "bck02.sl.dsgroot.int"
            ValueType = 'String'
            Force = $true
            DependsOn = '[Package]Networker'
        } 

     }     
}
cd D:\DSC_configuration
Tooling 

﻿Configuration LCMConfig_push
{

    param
    (
    [Parameter(Mandatory=$false)]
    [string[]]$ComputerName
    )

    node $ComputerName
     {
         LocalConfigurationManager
         {
            RebootNodeIfNeeded = $false
            AllowModuleOverwrite = $true
            ConfigurationMode = "ApplyAndAutoCorrect"
            ConfigurationModeFrequencyMins = 15
RefreshFrequencyMins = 30
            ConfigurationID = "5cdd056a-aca2-499a-ba52-036ac004058f"
            RefreshMode = 'Push'
            #DownloadManagerName = 'WebDownloadManager'
            #DownloadManagerCustomData = @{
                #ServerUrl = 'http://pullserver:8080/PSDSCPullServer.svc'
                #AllowUnsecureConnection = 'true' 
            #}
        } 
        
   
    }     
}
$computername = (get-content D:\DSC\hostname-pull.txt)
cd D:\DSC_configuration
LCMConfig_push -computername localhost
Set-DscLocalConfigurationManager -Path .\LCMConfig_push -Verbose -force
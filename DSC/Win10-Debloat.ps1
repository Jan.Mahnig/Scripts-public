configuration Win10-Debloat
{
    param
    (
    [Parameter(Mandatory=$true)]
    [string[]]$ComputerName
    )

    Import-DscResource -ModuleName PSDesiredStateConfiguration
    node $ComputerName
    {
        LocalConfigurationManager {
            ConfigurationModeFrequencyMins = 15
            ConfigurationMode = "ApplyAndAutocorrect"
        } 

################################################################
#######removal of all appxpackages which are not whitelisted####
################################################################
        Script Disable-AppxPackages
        {
            TestScript = {		
				[regex]$WhitelistedApps = 'Microsoft.Paint3D|Microsoft.WindowsCalculator|Microsoft.WindowsStore|Microsoft.Windows.Photos'
				if ((Get-AppxPackage -AllUsers | Where-Object {$_.Name -NotMatch $WhitelistedApps}).count -eq 0){
					Return $true
					Write-Verbose "true"
				}
				else {
					#config doesn't match
					Write-Verbose "false"
					Return $false
				}
			}

            SetScript = {
				Write-Verbose "setscript"
				[regex]$WhitelistedApps = 'Microsoft.Paint3D|Microsoft.WindowsCalculator|Microsoft.WindowsStore|Microsoft.Windows.Photos'
				Get-AppxPackage -AllUsers | Where-Object {$_.Name -notMatch $WhitelistedApps} | Remove-AppxPackage -ErrorAction SilentlyContinue
				Get-AppxProvisionedPackage -Online | Where-Object {$_.PackageName -notMatch $WhitelistedApps} | Remove-AppxProvisionedPackage -Online -ErrorAction SilentlyContinue 

            }
            GetScript = {
            @{
                Result = $true
                }
            }
       
        }

        Script Contact-support
        {
            TestScript = {		
				if (Get-WindowsCapability -online | where {$_.Name -like '*ContactSupport*'}){       
                    #config doesn't match, start "setscript"
                    Write-Verbose "test false"
					Return $false
				}
				else {
                    #config match, do nothing
                    Write-Verbose "test true"
					Return $true
				}
			}
            SetScript = {
                Get-WindowsCapability -online | where {$_.Name -like '*ContactSupport*'} | Remove-WindowsCapability �online
	            }
            GetScript = {
            @{
                Result = $true
                }
            }
       
        }

        Script QuickAssist
        {
            TestScript = {		
				if (Get-WindowsCapability -online | where {$_.Name -like '*QuickAssist*'}){       
                    #config doesn't match, start "setscript"
					Return $false
				}
				else {
                    config match, do nothing
					Return $true
				}
			}
            SetScript = {
                Get-WindowsCapability -online | where {$_.Name -like '*QuickAssist*'} | Remove-WindowsCapability �online
	            }
            GetScript = {
            @{
               Result = $true
                }
            }
       }
        # Prevents "Suggested Applications" returning
        Registry DisableWindowsConsumerFeatures 
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent"
            ValueName = 'DisableWindowsConsumerFeatures'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        } 

        Registry WindowsStore-autodownload 
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\WindowsStore"
            ValueName = 'AutoDownload'
            ValueData = 2
            ValueType = 'Dword'
            Force = $true
        } 

        Registry EnableFirstLogonAnimation01
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
            ValueName = 'EnableFirstLogonAnimation '
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 

        Registry EnableFirstLogonAnimation02
        {
            Ensure = 'Present'
            Key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
            ValueName = 'EnableFirstLogonAnimation '
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 

        #just a example of file resource
        File Test-file
        {
            Ensure          = "Present"
            DestinationPath = "c:\testDSC.txt"
            Type            = "File"
            Contents = "test DSC pull"
        }             
               
        #disable telemetry
        Registry AllowTelemetry01
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection"
            ValueName = 'AllowTelemetry'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 
        Registry AllowTelemetry02 
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Policies\DataCollection"
            ValueName = 'AllowTelemetry'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        }
        Registry AllowTelemetry03
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\DataCollection"
            ValueName = 'AllowTelemetry'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        }

        Registry DisableBiometrics 
        {
           Ensure = "Present"
           Key = "HKLM:\SOFTWARE\Policies\Microsoft\Biometrics"
           ValueName = "Enabled"
           ValueData = "0"
           ValueType = "Dword" 
        }

        #disable SHODAN
        Registry AllowCortana  
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search"
            ValueName = 'AllowCortana'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 

        #HKCU ... tohle asi nebude fungovat
        Registry NoTileApplicationNotification   
        {
            Ensure = 'Present'
            Key = "HKCU:\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\PushNotifications"
            ValueName = 'NoTileApplicationNotification'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        } 

        #disable lockscreen
        Registry NoLockScreen   
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization"
            ValueName = 'NoTileApplicationNotification'
            ValueData = 1
            ValueType = 'Dword'
            Force = $true
        } 

        

       #enable windows update service
        service windowsupdate
        {
            name = "wuauserv"
            StartupType = "AUtomatic"
            State = 'Running'
        }
        #enable PSremoting
        service WinRM
        {
            Name = "WinRM"
            StartupType = "Automatic"
            State = 'Running'
        }

        #Disable windows services
        service DiagTrack
        {
            Name = "DiagTrack"
            StartupType = "Disabled"
            State = 'Stopped'
        }

        service DPS
        {
            Name = "DPS"
            StartupType = "Disabled"
            State = 'Stopped'
        }
        service dmwappushservice
        {
            Name = "dmwappushservice"
            StartupType = "Disabled"
            State = 'Stopped'
        }

        service XboxNetApiSvc
        {
            Name = "XboxNetApiSvc"
            StartupType = "Disabled"
            State = 'Stopped'
        }

        service XblGameSave
        {
            Name = "XblGameSave"
            StartupType = "Disabled"
            State = 'Stopped'
        }
        service XblAuthManager
        {
            Name = "XblAuthManager"
            StartupType = "Disabled"
            State = 'Stopped'
        }

        service WbioSrvc
        {
            Name = "WbioSrvc"
            StartupType = "Disabled"
            State = 'Stopped'
        }       
        
        service lfsvc
        {
            Name = "lfsvc"
            StartupType = "Disabled"
            State = 'Stopped'
        }    
        
        service MapsBroker
        {
            Name = "MapsBroker"
            StartupType = "Disabled"
            State = 'Stopped'
        }                    

      #Disable Game DVR and Game Bar
        Registry AllowgameDVR   
        {
            Ensure = 'Present'
            Key = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\GameDVR"
            ValueName = 'NoTileApplicationNotification'
            ValueData = 0
            ValueType = 'Dword'
            Force = $true
        } 
      #Remove 3D Objects
        Registry 3DObjects01   
        {
            Ensure = 'Absent'
            Key = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\"
            ValueName = '{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}'
            Force = $true
        }         
        Registry 3DObjects02 
        {
            Ensure = 'Absent'
            Key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace"
            ValueName = '{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}'
            Force = $true
        }        


    } 
}

cd D:\DSC_configuration
Win10-Debloat

$ErrorActionPreference = "SilentlyContinue"

Remove-Item c:\$RECYCLE.BIN\ -Recurse -Force -Confirm:$false 

(New-Object -ComObject Shell.Application).Namespace(0xA).items()  | ForEach-Object{ remove-item $_.path -Recurse -Confirm:$false -Verbose -ErrorAction SilentlyContinue} 
Remove-Item -Path "C:\ProgramData\Microsoft\Windows\WER" -Recurse -force -recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\WER\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\IECompatCache\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\IECompatUaCache\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\IEDownloadHistory\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\INetCache\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue
Remove-Item -Path "C:\Users\*\AppData\Local\Microsoft\Windows\INetCookies\*" -Force -Recurse -Verbose -ErrorAction SilentlyContinue

   
Remove-Item "C:\Users\*\AppData\Local\Microsoft\Windows\Temporary Internet Files\"   -Force -Verbose -ErrorAction SilentlyContinue -recurse 
Remove-Item "C:\Users\*\AppData\Local\Microsoft\Terminal Server Client\Cache\*.*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue -recurse 
Remove-Item "C:\users\*\AppData\Local\Temp\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue
    #Windows update
Get-Service -Name wuauserv | Stop-Service -Force -Verbose -ErrorAction SilentlyContinue
Remove-Item"C:\Windows\SoftwareDistribution\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue -Recurse 
Get-Service -Name wuauserv | Start-Service

#windows temp
Remove-Item "C:\Windows\Temp\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue

#IIS logs
Remove-Item "C:\inetpub\logs\LogFiles\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue
Remove-Item "c:\windows\prefetch\*" -Recurse -Force  -Verbose -ErrorAction SilentlyContinue
Remove-Item "C:\WINDOWS\Downloaded Program Files\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue
Remove-Item "C:\Windows\Offline Web Pages\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue
Remove-Item "C:\WINDOWS\SoftwareDistribution\Download\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue 
compact /s:c:\temp /c /i 2 |out-null
compact /s:c:\admin /c /i 2 |out-null
compact /s:c:\tools /c /i 2 |out-null
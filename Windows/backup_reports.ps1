###########################################################################################
# Title:	Networker backup reporting 
# Filename:	backup_reports.ps1     	
# Created by:	Jan Mahnig		
# Date:	15.5.2018			
# Version    1.0  					
###########################################################################################
if (!(test-path c:\scripts\windows\log\)){
    new-item c:\scripts\windows\log\ -ItemType Directory
}

if (!(test-path c:\scripts\windows\archive\)){
    new-item c:\scripts\windows\archive\ -ItemType Directory
}


start-transcript -path c:\scripts\windows\log\backup_reports.txt
Import-Module -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue
$filelocation  = "C:\Scripts\windows\archive\backups_"+(get-date -UFormat "%d.%m.%y")+".html"
$subject = "Networker daily backup report "+(get-date -UFormat %d.%m.%y)
$smtpServer = "" 
$mailfrom = ""
$to_error = ""
$to = ""
$style = "<style>BODY{font-family: Arial; font-size: 7pt;}"
$style = $style + "TABLE{border: 1px solid black; border-collapse: collapse;}"
$style = $style + "TH{border: 1px solid black; background: #dddddd; padding: 5px; }"
$style = $style + "TD{border: 1px solid black; padding: 5px; }"
$style = $style + "</style>"

####################################
# VMware VirtualCenter server name #
####################################
$vcserver = $args[0]

connect-viserver $vcserver -WarningAction SilentlyContinue

$test_vc = $global:DefaultVIServer
if($test_vc -eq $null) {
	Send-MailMessage -From $mailfrom -To $to_error  -Subject "Backup report failed" -SmtpServer $smtpServer -usessl
	stop-transcript
	exit
}

$hosts= "Windows|2008|2012|2016"
$vms = (get-vm |Where-Object {$_.Guest -match $hosts -AND $_.GuestId -match $hosts -AND $_.PowerState -eq "poweredON" -AND $_.name -notlike "*mgt*" -AND $_.name -notlike "GBSLVDWTEST*" -AND $_.name -notlike "*DSK0**"}).name |Sort-Object -unique
Set-Location  "C:\Program Files\EMC NetWorker\nsr\bin"
$csv = mminfo -xc* -s bck02 -a -r 'client,name,group,level,sumsize,savetime(18),sscomp(25),volretent(25),ssflags' -q 'savetime>1 days ago' -otR |ConvertFrom-Csv -Delimiter "*"


$array_os = @()
$csv |Where-Object {$_.group -like "*windows hosts*" -AND $_.ssflags -ne "vF"} | ForEach-Object {
    $object = "" | Select-Object Client, Name, Level, SumSize, Completed, Valid, Finished, Flag
    $object.Client = $_.client
    $object.Name = $_.name
    $object.Level = $_.level
    $object.sumsize = $_.'sum-size'
    $object.Completed = $_.'ss-completed'
    $object.flag = $_.ssflags
    if ($_.ssflags -match "vF") {
        $object.valid = $true
        $object.finished = $true
    }
    else {
        $object.valid = "N/A"
        $object.finished = "N/A"
    }
    if ($_.name -notlike "*\\?\VOLUME*" -AND $_.name -notlike "*DISASTER_RECOVERY*" -AND $_.name -notlike "WINDOWS ROLES AND FEATURES:\"){
        $array_os += $object      
    }   
}


if ($csv){
    $missed_backups = (Compare-Object -ReferenceObject ($vms) -DifferenceObject(($csv |Where-Object {$_.group -like "*windows hosts*" -AND $_.ssflags -eq "vF"}).client |Sort-Object -Unique) ).inputobject
}
else {
    $missed_backups = $vms
}

$array_sql = @()
$csv |Where-Object {$_.group -like "*windows mssql*" -and $_.level -ne "txnlog" -AND $_.ssflags -ne "vF"} | ForEach-Object {
    $object = "" | Select-Object Client, Name, Level, SumSize, Completed, Valid, Finished, Flag
    $object.Client = $_.client
    $object.Name = $_.name
    $object.Level = $_.level
    $object.sumsize = $_.'sum-size'
    $object.Completed = $_.'ss-completed'
    $object.flag = $_.ssflags
    if ($_.ssflags -match "vF") {
        $object.valid = $true
        $object.finished = $true
    }
    $array_sql += $object
}


ConvertTo-Html -title "Softlayer backup report" -Head $style -body "<H1>Softlayer backup report</H1>"  | Out-File $filelocation

if ($array_os){
    $array_os | Sort-Object Client | ConvertTo-Html  -body "<H2>Failed Windows OS and file backups</H2>" | Out-File -Append $filelocation
}
else {
    "<H3>No failed Windows OS and file backups</H3>" | Out-File -Append $filelocation
}

if ($array_sql){
    $array_sql | Sort-Object Client | ConvertTo-Html  -body "<H2>Failed MSSQL backups</H2>" | Out-File -Append $filelocation
}
else {
    "<H3>No failed MSSQL backups</H3>" | Out-File -Append $filelocation
}

if ($missed_backups){
    "<H3>Missed daily backups</H3>"  | Out-File -Append $filelocation
    $missed_backups |ForEach-Object {
        $_ + "<br>" |Out-File -Append $filelocation
        } 
}
else {
    "<H3>No missed dialy backups</H3>" | Out-File -Append $filelocation
}

if ($missed_backups -OR $array_os -OR $array_sql){
    "<br><h3>Explanation for ssflags<br></h3>C = Continued<br>v = valid<br>r = purged (recoverable)<br>E = Eligible for recycling<br>N = NDMP generated<br>i = incomplete<br>R = Raw<br>P = snapshot<br>K = cover<br>I = In-progress<br>F = Finished (ended)<br>k = checkpoint restart enabled<br><br><br>sent from $env:computername"|   out-file $filelocation -Append
    [System.Net.ServicePointManager]::ServerCertificateValidationCallback = { return $true }

    if (test-path $filelocation){
        Send-MailMessage -From $mailfrom -To $to -Body (get-content $filelocation |out-string) -Attachments $filelocation -Subject $subject -SmtpServer $smtpServer -UseSsl -BodyAsHtml
    }
    else{
        Send-MailMessage From $mailfrom -To $mailfrom  -Subject "Backup report failed" -SmtpServer $smtpServer -usessl
    }
}
stop-transcript


#################
# End Of script #
#################



